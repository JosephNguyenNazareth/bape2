package com.bape.model;



import com.bape.controler.ProcessEngineController;
import com.bape.event.ActivityClaimEvent;
import com.bape.event.ActivityCompleteEvent;
import com.bape.event.ActivityCreateEvent;
import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.ActivityProvokeEvent;
import com.bape.event.ProcessCreateEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskCreateEvent;
import com.bape.event.TaskStartEvent;
import com.bape.model.ActivityStateMachine.activityState;
import com.bape.model.TaskStateMachine.taskState;
import com.bape.type.ActivityDef;

public class ProcessStateMachine 
{
public PDGModel pdgModel;
	
	/**
	 * define task states as enumeration
	 */
	public enum processState
	{
		initial, inProgress, completed, aborted, finall
	}
	
	public processState state;
	public ProcessEngineController controler;
	
	public ProcessStateMachine()
	{
		this.pdgModel = PDGModel.getInstance();
		state = processState.initial;
	}
	
	
	public void init(Object event)
	{
		
		switch(state)
		{
			case initial: // source state
				if(event instanceof ProcessCreateEvent ) //event
				{
					setState(processState.inProgress);// target state
					((ProcessCreateEvent)event).getWID().currentState = state.name(); 
					pdgModel.processCreate(((ProcessCreateEvent) event).getWID(), state);// actions update PDG
					controler.createProcessUpdateView(((ProcessCreateEvent) event).getWID());// actions update view
					controler.createProcessMonitoringUpdateView(((ProcessCreateEvent) event).getWID());// actions update view
				}
				break;
				
			case inProgress:
				
					break;
				
				
			case completed: // automatic transition
				setState(processState.finall);
				break;
			
		}
	}
	
	
	public void setState(processState state)
	{
		this.state = state;
	}
	public processState getState()
	{
		return state;
	}
}
