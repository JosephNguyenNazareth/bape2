package com.bape.model;

import java.util.ArrayList;
import java.util.List;

import com.bape.controler.ProcessEngineController;
import com.bape.event.ArtifactUploadEvent;
import com.bape.event.DBUpdateEvent;
import com.bape.model.TaskStateMachine.taskState;

public class ArtifactStateMachine 
{
	public PDGModel pdgModel;
	public long taskID;
	public artifactState state;
	public ProcessEngineController controler;
	private static ArtifactStateMachine instance;
	
	/**
	 * define task states as enumeration
	 */
	public enum artifactState
	{
		initial, created, used
	}
	
	
	
	public ArtifactStateMachine()
	{
		this.pdgModel = PDGModel.getInstance();
		state = artifactState.initial;
	}
	
	 public static ArtifactStateMachine getInstance()
	   {
		   if(instance == null)
		   {
			   instance = new ArtifactStateMachine();
		   }
		   
		   	return instance;
		  
	   }
	 
	public void init(Object event)
	{
		
		switch(state)
		{
			case initial: // source state
				if(event instanceof ArtifactUploadEvent) //event
				{
					List<Long> wakeupTasks = new ArrayList<Long>();
					wakeupTasks = pdgModel.uploadArtifact(((ArtifactUploadEvent) event).getArtifact(), ((ArtifactUploadEvent) event).getWI());
					controler.artifactUpload(((ArtifactUploadEvent) event).getArtifact(), wakeupTasks, ((ArtifactUploadEvent) event).getWI());
					setState(artifactState.created); // target state 
					break;
				}
				
				if(event instanceof DBUpdateEvent) //event
				{
					List<Long> wakeupTasks = new ArrayList<Long>();
					wakeupTasks = pdgModel.produceArtifact(((DBUpdateEvent) event).getArtifact(), ((DBUpdateEvent) event).getprojID());
					controler.artifactDBUpload(((DBUpdateEvent) event).getArtifact(), wakeupTasks, ((DBUpdateEvent) event).getprojID());
					setState(artifactState.created); // target state 
					break;
				}
			case created: // source state
				
		}
	}
	
	
	public void setState(artifactState state)
	{
		this.state = state;
	}
	public artifactState getState()
	{
		return state;
	}
	
	public void settaskID(long taskID)
	{
		this.taskID = taskID;
	}
	public long getTaskID()
	{
		return taskID;
	}
}
