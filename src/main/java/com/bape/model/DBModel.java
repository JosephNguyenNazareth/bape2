package com.bape.model;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.io.fs.FileUtils;

import com.bape.model.PDGModel.PDG;
import com.bape.model.PDGModel.RelTypes;
import com.bape.type.ArtifactType;




public class DBModel 
{
			
			//create an object of SingleObject
		   private static DBModel instance; 

		   // private constructor so that this class cannot be
		   //instantiated
		   private DBModel()
		   {

		    	DB_PATH = new File("target/DB");
		   }

		   //Get the only object available
		   public static DBModel getInstance()
		   {
			   if(instance == null)
			   {
				   instance = new DBModel();
			   }
			   
			   	return instance;
			  
		   }
		   
	    
	   // public static final String DB_PATH = "target/DB";
	    public File DB_PATH = null;
	    
	    public String greeting;
	    long stat = 0;
	    Node wpNode, actorNode, activityNode ,taskNode; 

	    // START SNIPPET: vars
	    GraphDatabaseService graphDb;
	   
	   
	    // END SNIPPET: vars

	    // START SNIPPET: createReltype
	    public static enum DBRelTypes implements RelationshipType
	    {
	        BELONG 
	    }
	    // END SNIPPET: createReltype
	    public enum DB implements Label {
	    	Project, Artifact
	    }

	    public void initilizeDB()
	    {
	    	graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	Node projNode = graphDb.createNode(DB.Project);
	        	projNode.setProperty("Name", "Modify Testbench Wiring");
	        	projNode.setProperty("ID", "1");
	        	
	        	Node projNode1 = graphDb.createNode(DB.Project);
	        	projNode1.setProperty("Name", "Generic Wiring Modification");
	        	projNode1.setProperty("ID", "2");
	        	
	        	Node artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "FICD");
    			artifactNode.setProperty("Instance Name", "FICD1");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			Relationship rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "FICD");
    			artifactNode.setProperty("Instance Name", "FICD2");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode1, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "FD");
    			artifactNode.setProperty("Instance Name", "FD1");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "FD");
    			artifactNode.setProperty("Instance Name", "FD2");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode1, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Electrical Specification");
    			artifactNode.setProperty("Instance Name", "Electrical Specification1");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Electrical Specification");
    			artifactNode.setProperty("Instance Name", "Electrical Specification2");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode1, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Mechanic Need");
    			artifactNode.setProperty("Instance Name", "Mechanic Need1");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Components Available");
    			artifactNode.setProperty("Instance Name", "Components Available1");
    			artifactNode.setProperty("State", "Wt-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Bench File");
    			artifactNode.setProperty("Instance Name", "Bench File1");
    			artifactNode.setProperty("State", "Id-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Installation File");
    			artifactNode.setProperty("Instance Name", "Installation File1");
    			artifactNode.setProperty("State", "Md-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			
    			artifactNode = graphDb.createNode(DB.Artifact);
				artifactNode.setProperty("Name", "Purchase List");
    			artifactNode.setProperty("Instance Name", "Purchase List1");
    			artifactNode.setProperty("State", "Ed-Def");
    			artifactNode.setProperty("Usage", "T-S");
    			rel = artifactNode.createRelationshipTo(projNode, RelTypes.BELONG);
    			tx.success();
	        }
	    	
	        System.out.println( "Centeral Data Base is Initialized" );
			graphDb.shutdown();
	    } 
//--------------------------------------------------------------------------
	    public List<String> getProjIDs()
	    {
	    	graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
	        registerShutdownHook( graphDb );
	        List<String> projList = new ArrayList<String>();
	        projList.add("please select");
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	
           		ResourceIterator<Node> node = graphDb.findNodes(PDG.Process);
       			while(node.hasNext())
       			{
       				Node temp = node.next();
       				if(temp!=null)
       				{
       					projList.add(temp.getProperty("ID").toString());
       				}
       			}
	       		node.close();
	        	
    			tx.success();
	        }
	    	
	        System.out.println( "Centeral Data Base is Initialized" );
			graphDb.shutdown();
	    
	    	
	    	return projList;
	    }
//-----------------------------------------------------------------------------
	    public void produceArtifact(ArtifactType artifact)
	    {
	    	graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	
	        	Node artifactNode = graphDb.findNode(DB.Artifact, "Instance Name", artifact.instanceName.get(0));
	        	if(artifactNode!=null)
	        	{
	        		artifactNode = graphDb.createNode(DB.Artifact);
	        		artifactNode.setProperty("Name", artifact.type);
	        		artifactNode.setProperty("Instance Name", artifact.instanceName.get(0));
	        		artifactNode.setProperty("Usage", "");
	        	}
    			artifactNode.setProperty("State", artifact.state);
    			
    			
    			tx.success();
	        }
	    	
	        System.out.println( "Centeral Data Base is Initialized" );
			graphDb.shutdown();
	    
	    	
	    }
	    
//*******CLEAR DB *******************************************************
	    
	    public void clearDb()
	    {
	        try
	        {
	            FileUtils.deleteRecursively( new File( "target/DB" ) );
	        }
	        catch ( IOException e )
	        {
	            throw new RuntimeException( e );
	        }
	    }
	    
	    void shutDown()
	    {
	        System.out.println("Schema is Created");
	        System.out.println( "Shutting down database ..." );
	        // START SNIPPET: shutdownServer
	        graphDb.shutdown();
	        // END SNIPPET: shutdownServer
	    }

	    // START SNIPPET: shutdownHook
	    static void registerShutdownHook( final GraphDatabaseService graphDb )
	    {
	        // Registers a shutdown hook for the Neo4j instance so that it
	        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
	        // running application).
	        Runtime.getRuntime().addShutdownHook( new Thread()
	        {
	            @Override
	            public void run()
	            {
	                graphDb.shutdown();
	            }
	        } );
	    } 
	    
	    
	    
	    
}
