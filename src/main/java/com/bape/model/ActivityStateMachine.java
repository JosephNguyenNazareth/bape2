package com.bape.model;



import com.bape.controler.ProcessEngineController;
import com.bape.event.ActivityClaimEvent;
import com.bape.event.ActivityCompleteEvent;
import com.bape.event.ActivityCreateEvent;
import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.ActivityProvokeEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskCreateEvent;
import com.bape.event.TaskStartEvent;
import com.bape.model.TaskStateMachine.taskState;
import com.bape.type.ActivityDef;

public class ActivityStateMachine 
{
public PDGModel pdgModel;
	
	/**
	 * define task states as enumeration
	 */
	public enum activityState
	{
		initial, created, inProgress, completed, finall
	}
	
	public activityState state;
	ProcessEngineController controler;
	
	public ActivityStateMachine()
	{
		this.pdgModel = PDGModel.getInstance();
		state = activityState.initial;
	}
	
	
	public void init(Object event) {
		switch(state) {
			case initial: // source state
				if(event instanceof ActivityCreateEvent ) { //event
					setState(activityState.inProgress);// target state
					pdgModel.activityCreate(((ActivityCreateEvent) event).getWID(), state); // action to update PDG
					controler.createActivityUpdateView(((ActivityCreateEvent) event).getWID()); // action to update view
				}
				break;
			case created: // source state
				if(event instanceof ActivityClaimEvent) { //event
					setState(activityState.inProgress); // target state
					pdgModel.activityClaim(((ActivityClaimEvent) event).getWI(),((ActivityClaimEvent) event).getActor(), state); // action to update PDG
					controler.activityClaimUpdateView(((ActivityClaimEvent) event).getWI(), ((ActivityClaimEvent) event).getActor()); // action to update view
					
				}
				break;
			case inProgress:
				if(event instanceof ActivityCreateOptionEvent) { //event
					setState(activityState.inProgress); // target state
					pdgModel.activityCreateOption(((ActivityCreateOptionEvent) event).getWID(), ((ActivityCreateOptionEvent) event).getOption());// actions update PDG
					controler.activityCreateOptionView(((ActivityCreateOptionEvent) event).getWID());// actions update view
					break;
				}
				if(event instanceof ActivityCompleteEvent) { //event
					if (pdgModel.verifyActivityPostCond(((ActivityCompleteEvent) event).getWI())) { //Gaurd
						setState(activityState.completed);// target state 
						pdgModel.activityComplete(((ActivityCompleteEvent) event).getWI(), state);// action to update PDG
						controler.activityCompleteView(((ActivityCompleteEvent) event).getWI());// action to update view
						
					}
					break;
					}
			case completed: // automatic transition
				setState(activityState.finall);
				break;
			}
	}
	
	
	public void setState(activityState state)
	{
		this.state = state;
	}
	public activityState getState()
	{
		return state;
	}
}
/*if(event instanceof ActivityProvokeEvent ) //event
{
	setState(activityState.created);// target state
	pdgModel.activityCreate1(((ActivityProvokeEvent) event).getWID(), ((ActivityProvokeEvent) event).getID(), state);// actions update PDG
	controler.createActivityUpdateView1(((ActivityProvokeEvent) event).getWID().role);// actions update view
}
break;*/