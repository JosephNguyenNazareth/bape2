package com.bape.model;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.neo4j.graphdb.Transaction;

import com.bape.type.generateXML;
import com.bape.type.humanResourceType;


public class OrganizationalModel 
{
	//create an object of SingleObject
	   private static OrganizationalModel instance; 

	   // private constructor so that this class cannot be
	   //instantiated
	   private OrganizationalModel()
	   {}

	   //Get the only object available
	   public static OrganizationalModel getInstance()
	   {
		   if(instance == null)
		   {
			   instance = new OrganizationalModel();
		   }
		   
		   	return instance;
		  
	   }
	 
//---------------------------------------------------------------------------------------------------------------------	   
	public List<humanResourceType> createLogIn()
	{
		
		
		//Access to the Human Resource repos
				String fileName = "resources/roles";
				List<humanResourceType> humanRes = new ArrayList<humanResourceType>();
				//HashMap<String, String> actorMap = new HashMap<String, String>();
				
				String line = null;
				//int actorNum=0;
				try
				{
		 	    	// FileReader reads text files in the default encoding.
		 	    	FileReader fileReader =  new FileReader(fileName);         	
		 	    	// Always wrap FileReader in BufferedReader.
		 	    	BufferedReader bufferedReader =  new BufferedReader(fileReader);
		 	    	//int i=0;
		 	    	
		 	    	
		 	    	while((line = bufferedReader.readLine()) != null) 
		 	    	{
		 
		 	    		String[] parts = line.split("=");
		 	    		//actorMap.put(parts[0], parts[1]);
		 	    		humanResourceType hr = new humanResourceType();
		 	    		hr.actor = parts[0];
		 	    		hr.role = parts[1];
		 	    		hr.password = parts[2];
		 	    		hr.email=parts[3];
		 	    		hr.tel = parts[4];
						humanRes.add(hr);
		 	    		//actorNum++;
		 	    	//	i++;
		 	    		
		 	    	}
		 	    	bufferedReader.close();
		 	    	
			    	
				}
				catch(IOException ex) 
				{
					System.out.println("Error reading file '" + fileName + "'");                  
				}
				return humanRes;
	}
//---------------------------------------------------------------------------------------------------------------------	 	
	public void createRoleFragment(final String role)
	{
		// check that the process fragment exist or not
				File dir = new File("resources");
				File[] matchingFiles = dir.listFiles(new FilenameFilter() {
				    public boolean accept(File dir, String name) {
				        return name.startsWith(role) && name.endsWith("xml");
				    }
				});
				
				// process fragment doesn not exist
				if(matchingFiles.length != 0)
				{
					generateXML pfxml = new generateXML("resources/" + role + ".xml");
				}
	}
}
