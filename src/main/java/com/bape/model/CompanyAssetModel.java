package com.bape.model;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bape.type.ActivityDef;
import com.bape.type.generateXML;
import com.bape.type.humanResourceType;



public class CompanyAssetModel 
{
	ActivityDef workDef;
	generateXML caxml = new generateXML("resources/CompanyAsset.xml");
	generateXML omxml = new generateXML("resources/OrganizationalModel1.xml");
	
	public  CompanyAssetModel ( )
	{
		//this.role = role;
		//createTaskList();
	}
//------------------------------------------------------------------------------------------------------------------------------------------
	public List<Object> reloadProcessList(humanResourceType hres)
	{
		List<Object> processList = new ArrayList<Object>();
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		if(hres.role.equals("Project Manager"))
		{
			for(int i = 0; i<xmlProcessList.getLength(); i++)
			{
				processList.add(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString());
			}
		}
		else
		{
			NodeList xmlRoleList =  caxml.doc.getElementsByTagName("role");
			for(int i = 0; i<xmlRoleList.getLength(); i++)
			{
				if(xmlRoleList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(hres.role))
				{
					//if(xmlRoleList.item(i).getParentNode().getNodeName().equals("process"))
					{
						processList.add(xmlRoleList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue().toString());
					}
				}
			}
			
		}
		return processList;
		
	}
//------------------------------------------------------------------------------------------------------------------
	public List<Object> reloadRoleList(humanResourceType hres)
	{
		List<Object> roleList = new ArrayList<Object>();
		NodeList xmlRoleList =  omxml.doc.getElementsByTagName("role");
		if(hres.role.equals("Project Manager"))
		{
			for(int i = 0; i<xmlRoleList.getLength(); i++)
			{
				roleList.add(xmlRoleList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString());
			}
		}
		
		return roleList;
		
	}
//------------------------------------------------------------------------------------------------------------------
	public List<Object> reloadOptionList(String process)
	{
		List<Object> optionList = new ArrayList<Object>();
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				NodeList optionchilds = xmlProcessList.item(i).getChildNodes();
				
				for (int j = 0; j < optionchilds.getLength(); j++) 	
				{
					if(optionchilds.item(j).getNodeName().equals("option"))
					{
						optionList.add(optionchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString());
					}
				}
			}
			
		}
		
		
		return optionList;
		
	}
//------------------------------------------------------------------------------------------------------------------
	public void uploadProcessList(List<Object> processList)
	{
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(processList.contains(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString()))
			{
				processList.remove(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString());
			}
		}
		if(!processList.isEmpty())
		{
			Node root = caxml.doc.getElementsByTagName("processes").item(0);
			for(int i=0; i<processList.size(); i++)
			{
				Element process = caxml.doc.createElement("process");
				process.setAttribute("name",(String) processList.get(i));
				root.appendChild(process);
			}
		}
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
        
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
//------------------------------------------------------------------------------------------------------------------
	public void deleteProcess(String process)
	{
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				Node processParent = xmlProcessList.item(i).getParentNode();
				Node processNode = xmlProcessList.item(i);
				processParent.removeChild(processNode);
			}
		}
		
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
        
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
//------------------------------------------------------------------------------------------------------------------
	public void uploadRoleList(String process, List<Object> roleList)
	{
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		Node processNode=null;
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				processNode = xmlProcessList.item(i);
				NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
				
				for (int j = 0; j < rolechilds.getLength(); j++) 	
				{
					if(rolechilds.item(j).getNodeName().equals("role"))
					{
						if(roleList.contains(rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue()))
						{
							roleList.remove(rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue());
						}
					}
				}
			}
		}
		if(!roleList.isEmpty())
		{
			for(int i=0; i<roleList.size(); i++)
			{
				Element role = caxml.doc.createElement("role");
				role.setAttribute("name",(String)roleList.get(i));
				processNode.appendChild(role);
			}
		}
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
        
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
//------------------------------------------------------------------------------------------------------------------------------------------
		public List<Object> reloadRoleList(String process)
		{
			List<Object> roleList = new ArrayList<Object>();
			NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
			
			for(int i = 0; i<xmlProcessList.getLength(); i++)
			{
				if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
				{
					NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
					
					for (int j = 0; j < rolechilds.getLength(); j++) 	
					{
						if(rolechilds.item(j).getNodeName().equals("role"))
						{
							
							roleList.add(rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString());
							
						}
					}
				}
			}
			return roleList;
			
		}
//------------------------------------------------------------------------------------------------------------------
	public void deleteRole(String process, String role)
	{
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
				Node processNode = xmlProcessList.item(i);
				for (int j = 0; j < rolechilds.getLength(); j++) 	
				{
					if(rolechilds.item(j).getNodeName().equals("role") && 
						rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role))
					{
						processNode.removeChild(rolechilds.item(j));
						
					}
				}
			}
		}
		
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
	    
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty(
	                "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}		
//------------------------------------------------------------------------------------------------------------------
	public void uploadOptionList(String process, List<Object> optionList)
	{
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		Node processNode=null;
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				processNode = xmlProcessList.item(i);
				NodeList optionchilds = xmlProcessList.item(i).getChildNodes();
				
				for (int j = 0; j < optionchilds.getLength(); j++) 	
				{
					if(optionchilds.item(j).getNodeName().equals("option"))
					{
						if(optionList.contains(optionchilds.item(j).getAttributes().getNamedItem("name").getNodeValue()))
						{
							optionList.remove(optionchilds.item(j).getAttributes().getNamedItem("name").getNodeValue());
						}
					}
				}
			}
		}
		if(!optionList.isEmpty())
		{
			for(int i=0; i<optionList.size(); i++)
			{
				Element option = caxml.doc.createElement("option");
				option.setAttribute("name",(String)optionList.get(i));
				processNode.appendChild(option);
			}
		}
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
	    
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty(
	                "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
}
//------------------------------------------------------------------------------------------------------------------
	public void deleteOption(String process, String option)
	{
		NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
		
		for(int i = 0; i<xmlProcessList.getLength(); i++)
		{
			if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
			{
				NodeList optionchilds = xmlProcessList.item(i).getChildNodes();
				Node processNode = xmlProcessList.item(i);
				for (int j = 0; j < optionchilds.getLength(); j++) 	
				{
					if(optionchilds.item(j).getNodeName().equals("role") && 
							optionchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString().equals(option))
					{
						processNode.removeChild(optionchilds.item(j));
						
					}
				}
			}
		}
		
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
        
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
//-------------------------------------------------------------------------------------------------------------------------------------------	
		public List<Object> reloadArtifactList()
		{
			List<Object> artifactList = new ArrayList<Object>();
			NodeList xmlArtifactList =  caxml.doc.getElementsByTagName("artifact");
			
			for(int i = 0; i<xmlArtifactList.getLength(); i++)
			{
				if(!artifactList.contains(xmlArtifactList.item(i).getAttributes().getNamedItem("name").getNodeValue()))
				{
							
					artifactList.add(xmlArtifactList.item(i).getAttributes().getNamedItem("name").getNodeValue());
							
				}
			}
			return artifactList;
			
		}	
		
		public List<Object> reloadArtifactList(String process, String role)
		{
			List<Object> artifactList = new ArrayList<Object>();
			NodeList xmlRoleList =  caxml.doc.getElementsByTagName("role");
			for(int i = 0; i<xmlRoleList.getLength(); i++)
			{
				if(xmlRoleList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue().toString().equals(process)
						& xmlRoleList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role)	)
				{
					NodeList artifactchilds = xmlRoleList.item(i).getChildNodes();
					for (int j = 0; j < artifactchilds.getLength(); j++) 	
					{
						if(artifactchilds.item(j).getNodeName().equals("artifact"))
						{
							artifactList.add(artifactchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString());
						}
					}
				break;
				}
				
			}
			return artifactList;
			
		}	
//---------------------------------------------------------------------------------------------------
		public void uploadArtifactList(String process, String role, List<Object> artifactList)
		{
			NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
			Node roleNode=null;
			for(int i = 0; i<xmlProcessList.getLength(); i++)
			{
				if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
				{
					NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
					for (int j = 0; j < rolechilds.getLength(); j++) 	
					{
						if(rolechilds.item(j).getNodeName().equals("role"))
						{		
							if(rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role))
							{
								roleNode = rolechilds.item(j);
								NodeList artifactchilds = rolechilds.item(j).getChildNodes();
								for(int k = 0; k < artifactchilds.getLength(); k++) 	
								{
									if(artifactchilds.item(k).getNodeName().equals("artifact"))
									{	
										if(artifactList.contains(artifactchilds.item(k).getAttributes().getNamedItem("name").getNodeValue()))
										{
											artifactList.remove(artifactchilds.item(k).getAttributes().getNamedItem("name").getNodeValue());
										}
									}
								}
							}
						}
					}
					break;
				}
			}
			if(!artifactList.isEmpty())
			{
				for(int i=0; i<artifactList.size(); i++)
				{
					Element artifact = caxml.doc.createElement("artifact");
					artifact.setAttribute("name",(String)artifactList.get(i));
					roleNode.appendChild(artifact);
				}
			}
			// write the new content into process fragment.xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = null;
		
	        
			try {
				transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	            transformer.setOutputProperty(
	                    "{http://xml.apache.org/xslt}indent-amount", "3");
				
			} catch (TransformerConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			DOMSource source = new DOMSource(caxml.doc);
			StreamResult result = new StreamResult(new File(caxml.processPath));
			try {
				transformer.transform(source, result);
				caxml.doc.getDocumentElement().normalize();
			} catch (TransformerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
//-------------------------------------------------------------------------------------------------------------------------------------
		public void deleteArtifact(String process, String role, String artifact)
		{
			NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
			Node roleNode=null;
			for(int i = 0; i<xmlProcessList.getLength(); i++)
			{
				if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
				{
					NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
					for (int j = 0; j < rolechilds.getLength(); j++) 	
					{
						if(rolechilds.item(j).getNodeName().equals("role"))
						{		
							if(rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role))
							{
								roleNode = rolechilds.item(j);
								NodeList artifactchilds = rolechilds.item(j).getChildNodes();
								for(int k = 0; k < artifactchilds.getLength(); k++) 	
								{
									if(artifactchilds.item(k).getNodeName().equals("artifact"))
									{	
										roleNode.removeChild(artifactchilds.item(k));
									}
									break;
								}
							}
							break;
						}
					}
					break;
				}
			}
		
			// write the new content into process fragment.xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = null;
		
	        
			try {
				transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	            transformer.setOutputProperty(
	                    "{http://xml.apache.org/xslt}indent-amount", "3");
				
			} catch (TransformerConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			DOMSource source = new DOMSource(caxml.doc);
			StreamResult result = new StreamResult(new File(caxml.processPath));
			try {
				transformer.transform(source, result);
				caxml.doc.getDocumentElement().normalize();
			} catch (TransformerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
//-------------------------------------------------------------------------------------------------------------------------------------------	
		public List<Object> reloadStateList(String artifact)
		{
			List<Object> stateList = new ArrayList<Object>();
			NodeList xmlStateList =  caxml.doc.getElementsByTagName("state");
			
			for(int i = 0; i<xmlStateList.getLength(); i++)
			{
				if(xmlStateList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue().equals(artifact))
				{
					if(!stateList.contains(xmlStateList.item(i).getAttributes().getNamedItem("name").getNodeValue()))
					{
						stateList.add(xmlStateList.item(i).getAttributes().getNamedItem("name").getNodeValue());
					}
				}
			}
			return stateList;
			
		}	
		
		public List<Object> reloadStateList(String process, String role, String artifact)
		{
			List<Object> stateList = new ArrayList<Object>();
			NodeList xmlRoleList =  caxml.doc.getElementsByTagName("role");
			for(int i = 0; i<xmlRoleList.getLength(); i++)
			{
				if(xmlRoleList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue().toString().equals(process)
						& xmlRoleList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role)	)
				{
					NodeList artifactchilds = xmlRoleList.item(i).getChildNodes();
					for (int j = 0; j < artifactchilds.getLength(); j++) 	
					{
						if(artifactchilds.item(j).getNodeName().equals("artifact") && 
								artifactchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().equals(artifact) )
						{
							NodeList statechilds = artifactchilds.item(j).getChildNodes();
							for (int k = 0; k < statechilds.getLength(); k++) 	
							{
								if(statechilds.item(k).getNodeName().equals("state"))
								{
									stateList.add(statechilds.item(k).getAttributes().getNamedItem("name").getNodeValue().toString());
								}
							}
						}
					}
					break;
				}
				
			}
			return stateList;
			
		}	
//---------------------------------------------------------------------------------------------------------
		public void uploadStateList(String process, String role, String artifact, List<Object> stateList)
		{
			
			NodeList xmlProcessList =  caxml.doc.getElementsByTagName("process");
			Node artifactNode=null;
			for(int i = 0; i<xmlProcessList.getLength(); i++)
			{
				if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(process))
				{
					NodeList rolechilds = xmlProcessList.item(i).getChildNodes();
					for (int j = 0; j < rolechilds.getLength(); j++) 	
					{
						if(rolechilds.item(j).getNodeName().equals("role"))
						{		
							if(rolechilds.item(j).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role))
							{
								NodeList artifactchilds = rolechilds.item(j).getChildNodes();
								for(int k = 0; k < artifactchilds.getLength(); k++) 	
								{
									if(artifactchilds.item(k).getNodeName().equals("artifact") &&
										artifactchilds.item(k).getAttributes().getNamedItem("name").getNodeValue().equals(artifact))
									{	
										artifactNode = artifactchilds.item(k); 
										NodeList statechilds = artifactchilds.item(k).getChildNodes();
										for(int m=0; m<statechilds.getLength(); m++)
										{
											if(statechilds.item(m).getNodeName().equals("state") &&
												stateList.contains(statechilds.item(m).getAttributes().getNamedItem("name").getNodeValue())	)
											{
												stateList.remove(statechilds.item(m).getAttributes().getNamedItem("name").getNodeValue());
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if(!stateList.isEmpty())
			{
				for(int i=0; i<stateList.size(); i++)
				{
					Element state = caxml.doc.createElement("state");
					state.setAttribute("name",(String)stateList.get(i));
					artifactNode.appendChild(state);
				}
			}
			// write the new content into process fragment.xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = null;
		
	        
			try {
				transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	            transformer.setOutputProperty(
	                    "{http://xml.apache.org/xslt}indent-amount", "3");
				
			} catch (TransformerConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			DOMSource source = new DOMSource(caxml.doc);
			StreamResult result = new StreamResult(new File(caxml.processPath));
			try {
				transformer.transform(source, result);
				caxml.doc.getDocumentElement().normalize();
			} catch (TransformerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}

//--------------------------------------------------------------------------------------------------------------------------------
	public void uploadActivityList(String process, String role, List<Object> activityList)
	{
		NodeList xmlRoleList =  caxml.doc.getElementsByTagName("role");
		Node roleNode=null;
		for(int i = 0; i<xmlRoleList.getLength(); i++)
		{
			if(xmlRoleList.item(i).getAttributes().getNamedItem("name").getNodeValue().toString().equals(role)
					&& xmlRoleList.item(i).getParentNode().getAttributes().getNamedItem("name").getNodeValue().toString().equals(process) )
			{
				NodeList activitychilds = xmlRoleList.item(i).getChildNodes();
				roleNode = xmlRoleList.item(i);
				for (int j = 0; j < activitychilds.getLength(); j++) 	
				{
					if(activitychilds.item(j).getNodeName().equals("Activity"))
					{	
						if(activityList.contains(activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue()))
						{
							activityList.remove(activitychilds.item(j).getAttributes().getNamedItem("name").getNodeValue());
						}
					}
				}
			}
		}
		
		if(!activityList.isEmpty())
		{
			for(int i=0; i<activityList.size(); i++)
			{
				Element activity = caxml.doc.createElement("activity");
				activity.setAttribute("name",(String)activityList.get(i));
				roleNode.appendChild(activity);
			}
		}
		// write the new content into process fragment.xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
	
	    
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty(
	                "{http://xml.apache.org/xslt}indent-amount", "3");
			
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(caxml.doc);
		StreamResult result = new StreamResult(new File(caxml.processPath));
		try {
			transformer.transform(source, result);
			caxml.doc.getDocumentElement().normalize();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}			
 
	
	
}
