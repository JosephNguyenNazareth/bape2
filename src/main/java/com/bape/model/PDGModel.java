package com.bape.model;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.neo4j.io.fs.FileUtils;

import com.bape.controler.ProcessEngineController;
import com.bape.model.ActivityStateMachine.activityState;
import com.bape.model.DBModel.DB;
import com.bape.model.ProcessStateMachine.processState;
import com.bape.model.TaskStateMachine.taskState;
import com.bape.type.ActivityDef;
import com.bape.type.ArtifactType;
import com.bape.type.ChangeRequest;
import com.bape.type.MonitType;
import com.bape.type.ProcessDef;
import com.bape.type.TaskType;
import com.bape.type.humanResourceType;




	public class PDGModel
	{
		
			//create an object of SingleObject
		   private static PDGModel instance; 

		   // private constructor so that this class cannot be
		   //instantiated
		   private PDGModel()
		   {}

		   //Get the only object available
		   public static PDGModel getInstance()
		   {
			   if(instance == null)
			   {
				   instance = new PDGModel();
			   }
			   
			   	return instance;
			  
		   }
		   
		   
		   
		public File PDG_PATH =null;
		public File DB_PATH =null;  
	    //public static final String PDG_PATH = "target/PDG";
	    //public static final String DB_PATH = "target/DB";
	    private DBModel dbModel = DBModel.getInstance();
	    public String greeting;
	    long stat = 0;
	    Node wpNode, actorNode, activityNode ,taskNode; 

	    // START SNIPPET: vars
	    GraphDatabaseService graphDb, graphDb1;
	   
	   
	    // END SNIPPET: vars

	    // START SNIPPET: createReltype
	    public static enum RelTypes implements RelationshipType
	    {
	       BELONG, DATA , PERFORM , USED, CHILED
	    }
	    // END SNIPPET: createReltype
	    public enum PDG implements Label {
	    	Process, Activity, Task, Artifact, Actor, Resource;
	    }
	    
//***********************************************************
	    public void initilizePDG()
	    {
	    	PDG_PATH = new File("target/PDG");
	    	DB_PATH = new File("target/DB");
	    }
	    	/*graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	        graphDb1 = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
	        registerShutdownHook( graphDb1 );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction txx = graphDb1.beginTx() )
	        {
	        	try (Transaction tx = graphDb.beginTx() )
		        {
	        		ResourceIterator<Node> node = graphDb1.findNodes(DB.Project);
        			while(node.hasNext())
        			{
        				Node temp = node.next();
        				if(temp!=null)
        				{
        				 
        				Node projNode = graphDb.createNode(PDG.Process);
        				projNode.setProperty("Name",temp.getProperty("Name"));
        				projNode.setProperty("ID", Long.valueOf(temp.getProperty("ID").toString()).longValue());
        				String[] activeOptions = {};
        				projNode.setProperty("Active Options", activeOptions);
        				}
        			}
        		node.close();
	        		
	        		 node = graphDb1.findNodes(DB.Artifact);
	        			while(node.hasNext())
	        			{
	        				Node temp = node.next();
	        				if(temp!=null)
	        				{
	        				
	        				Node artifactNode = graphDb.createNode(PDG.Artifact);
	        				artifactNode.setProperty("Name",temp.getProperty("Name"));
	            			artifactNode.setProperty("Instance Name", temp.getProperty("Instance Name"));
	            			artifactNode.setProperty("State", temp.getProperty("State"));
	            			artifactNode.setProperty("Usage", temp.getProperty("Usage"));
	        				}
	        			}
	        		node.close();
	        		
	        		
	        		tx.success();
		        }
	        	
	        	txx.success();
		        }
	        catch (Exception e) 
			{
				e.printStackTrace();
			}
	        System.out.println( "Pre-assumptions are Instancieted into PDG --> PDG Shutting down  ..." );
	        graphDb1.shutdown();
			graphDb.shutdown();
	    }*/
//************** CREAT DB **********************************
	   public void createActor(List<humanResourceType> hres)
	    {
	    	// START SNIPPET: startDb
		  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	for (int i =0 ; i<hres.size(); i++)
	    		{
	        		Node actorNode = graphDb.createNode(PDG.Actor);
	 	    		actorNode.setProperty("Role", hres.get(i).role);
	 	    		actorNode.setProperty("Name", hres.get(i).actor);
	 	    		actorNode.setProperty("Email", hres.get(i).email);
	 	    		actorNode.setProperty("Telephone", hres.get(i).tel);
	    		}
	        	
	        	/**
	        	 * Change Scenario ID1 produce FICD1
	        	 */
	        	Node artifactNode = graphDb.createNode(PDG.Artifact);
				artifactNode.setProperty("Name","Wiring Change Demand");
    			artifactNode.setProperty("Instance Name", "WCD1");
    			artifactNode.setProperty("State", "defined");
    			artifactNode.setProperty("Usage", "ToStart");
	        	
    			Node artifactNode1 = graphDb.createNode(PDG.Artifact);
				artifactNode1.setProperty("Name","Component Requirements");
    			artifactNode1.setProperty("Instance Name", "CR1");
    			artifactNode1.setProperty("State", "defined");
    			artifactNode1.setProperty("Usage", "ToStart");
    			
    			Node artifactNode11 = graphDb.createNode(PDG.Artifact);
				artifactNode11.setProperty("Name","Component Requirements");
    			artifactNode11.setProperty("Instance Name", "CR2");
    			artifactNode11.setProperty("State", "defined");
    			artifactNode11.setProperty("Usage", "ToStart");
    			
    			Node artifactNode2 = graphDb.createNode(PDG.Artifact);
				artifactNode2.setProperty("Name","Electrical Specification");
    			artifactNode2.setProperty("Instance Name", "ES1");
    			artifactNode2.setProperty("State", "defined");
    			artifactNode2.setProperty("Usage", "ToStart");
    			
    			Node artifactNode3 = graphDb.createNode(PDG.Artifact);
				artifactNode3.setProperty("Name","FICD");
    			artifactNode3.setProperty("Instance Name", "FICD1");
    			artifactNode3.setProperty("State", "outlined");
    			artifactNode3.setProperty("Usage", "ToStart");
    			
    			Node artifactNode4 = graphDb.createNode(PDG.Artifact);
				artifactNode4.setProperty("Name","Bench Specification");
    			artifactNode4.setProperty("Instance Name", "BS1");
    			artifactNode4.setProperty("State", "defined");
    			artifactNode4.setProperty("Usage", "ToStart");
    			
    		/*	Node artifactNode5 = graphDb.createNode(PDG.Artifact);
				artifactNode5.setProperty("Name","ICD");
    			artifactNode5.setProperty("Instance Name", "ICD1");
    			artifactNode5.setProperty("State", "defined");
    			artifactNode5.setProperty("Usage", "ToStart");
    			
    			Node artifactNode6 = graphDb.createNode(PDG.Artifact);
				artifactNode6.setProperty("Name","Electrical Design Model");
    			artifactNode6.setProperty("Instance Name", "EDM1");
    			artifactNode6.setProperty("State", "defined");
    			artifactNode6.setProperty("Usage", "ToStart");*/
    			
    			/*Node artifactNode7 = graphDb.createNode(PDG.Artifact);
				artifactNode7.setProperty("Name","Components");
    			artifactNode7.setProperty("Instance Name", "C1");
    			artifactNode7.setProperty("State", "defined");
    			artifactNode7.setProperty("Usage", "ToStart");
    			
    			Node artifactNode71 = graphDb.createNode(PDG.Artifact);
				artifactNode71.setProperty("Name","Components");
    			artifactNode71.setProperty("Instance Name", "C2");
    			artifactNode71.setProperty("State", "defined");
    			artifactNode71.setProperty("Usage", "ToStart");*/
    			
    			
    			/*Node artifactNode8 = graphDb.createNode(PDG.Artifact);
				artifactNode8.setProperty("Name","Install Component");
    			artifactNode8.setProperty("Instance Name", "IC1");
    			artifactNode8.setProperty("State", "defined");
    			artifactNode8.setProperty("Usage", "ToStart");
	        	
    			Node artifactNode81 = graphDb.createNode(PDG.Artifact);
				artifactNode81.setProperty("Name","Install Component");
    			artifactNode81.setProperty("Instance Name", "IC2");
    			artifactNode81.setProperty("State", "defined");
    			artifactNode81.setProperty("Usage", "ToStart");
    			
    			Node artifactNode9 = graphDb.createNode(PDG.Artifact);
				artifactNode9.setProperty("Name","Testbench Wired");
    			artifactNode9.setProperty("Instance Name", "TW1");
    			artifactNode9.setProperty("State", "defined");
    			artifactNode9.setProperty("Usage", "ToStart");*/
	        	
	        	/*Node artifactNode, artifactNode1;
 	    		Relationship rel, rel1;
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Project", "1");
	        	activityNode.setProperty("Name", "Define Instrum");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("Name", "Define Instrum Task");
	        	taskNode.setProperty("State", "S1");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "ID1");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "FICD1");
	        	rel = taskNode.createRelationshipTo(artifactNode, RelTypes.DATA);
	        	rel.setProperty("State", "Ed-Def");
	        	
 	    		actorNode = graphDb.createNode(PDG.Actor);
 	    		actorNode.setProperty("Role", "WT");
 	    		actorNode.setProperty("Name", "WT1");
 	    		actorNode.setProperty("Email", "WT1@mail.com");
 	    		actorNode.setProperty("Telephone", "13");
 	    		actorNode= graphDb.createNode(PDG.Actor);
 	    		actorNode.setProperty("Role", "MD");
 	    		actorNode.setProperty("Name", "MD1");
 	    		actorNode.setProperty("Email", "MD1@mail.com");
 	    		actorNode.setProperty("Telephone", "14");
 	    		actorNode = graphDb.createNode(PDG.Actor);
 	    		actorNode.setProperty("Role", "ED");
 	    		actorNode.setProperty("Name", "ED2");
 	    		actorNode.setProperty("Email", "ED2@mail.com");
 	    		actorNode.setProperty("Telephone", "15");
 	    		
 	    		actorNode = graphDb.createNode(PDG.Actor);
 	    		actorNode.setProperty("Role", "ID");
 	    		actorNode.setProperty("Name", "ID2");
 	    		actorNode.setProperty("Email", "ID2@mail.com");
 	    		actorNode.setProperty("Telephone", "16");
	        	
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Project", "1");
	        	activityNode.setProperty("Name", "Define Instrum");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("Name", "Define Instrum Task");
	        	taskNode.setProperty("State", "S1");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "ID1");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "FICD1");
	        	rel = taskNode.createRelationshipTo(artifactNode, RelTypes.DATA);
	        	rel.setProperty("State", "Ed-Def");
	        	
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Project", "1");
	        	activityNode.setProperty("Name", "Generate Bench File");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("Name", "Generate Bench File Task");
	        	taskNode.setProperty("State", "S2");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "ID2");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "FICD1");
	        	artifactNode1 = graphDb.findNode(PDG.Artifact, "Instance Name", "Bench File1");
	        	rel = artifactNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        	rel1 = taskNode.createRelationshipTo(artifactNode1, RelTypes.DATA);
	        	rel1.setProperty("State", "Ed-Def");
	        	
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Project", "1");
	        	activityNode.setProperty("Name", "Supply Component");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("Name", "Supply Component Task");
	        	taskNode.setProperty("State", "S3");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "WT1");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "Purchase List1");
	        	artifactNode1 = graphDb.findNode(PDG.Artifact, "Instance Name", "Components Available1");
	        	rel = artifactNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        	rel1 = taskNode.createRelationshipTo(artifactNode1, RelTypes.DATA);
	        	rel1.setProperty("State", "Ed-Def");
	        	
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Name", "Design Mechanic");
	        	activityNode.setProperty("Project", "1");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("State", "S4");
	        	taskNode.setProperty("Name", "Design Mechanic Task");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "MD1");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "Mechanical Need1");
	        	artifactNode1 = graphDb.findNode(PDG.Artifact, "Instance Name", "Installation File1");
	        	rel = artifactNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        	rel1 = taskNode.createRelationshipTo(artifactNode1, RelTypes.DATA);
	        	rel1.setProperty("State", "Ed-Def");
	        	
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Project", "1");
	        	activityNode.setProperty("Name", "Realize Wiring Diagram");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("State", "S5");
	        	taskNode.setProperty("Name", "Realize Wiring Diagram Task");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "ED2");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "FICD2");
	        	artifactNode1 = graphDb.findNode(PDG.Artifact, "Instance Name", "Electrical Specification2");
	        	rel = artifactNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        	rel1 = taskNode.createRelationshipTo(artifactNode1, RelTypes.DATA);
	        	rel1.setProperty("State", "Ed-Def");*/
	        	
	        	
	        	
 	    		/*
	        	activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Project", "1");
	        	activityNode.setProperty("Name", "Design Electric");
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("State", "Completed");
	        	taskNode.setProperty("Name", "Specify Mechanical Needs");
	        	activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	actorNode = graphDb.findNode(PDG.Actor, "Name", "ED1");
	        	actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", "FICD1");
	        	Node artifactNode1 = graphDb.findNode(PDG.Artifact, "Instance Name", "Mechanic Need1");
	        	Relationship rel = artifactNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        	Relationship rel1 = taskNode.createRelationshipTo(artifactNode1, RelTypes.DATA);
	        	rel1.setProperty("State", "Ed-Def");*/
	        	
	        	
	        	
	        tx.success();
	        tx.close();
	        }
	    	
	        System.out.println( "Oraganizational Model Instancieted into PDG --> PDG Shutting down  ..." );
			graphDb.shutdown();
	    	
	    }
//--------------------------------------------------------------------------------------------------------
	   public void processCreate(ProcessDef pd, processState state)
	    {
	    	// START SNIPPET: startDb
		  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        
	        	// construct the Activity node
	        	Date date = new Date();
	        	Node processNode = graphDb.createNode(PDG.Process);
	        	processNode.setProperty("Name", pd.name);
	        	processNode.setProperty("ID", String.valueOf(pd.processId));
	        	processNode.setProperty("State", state.name());
	        	processNode.setProperty("Start Date", pd.startDate.format(date));
	        	processNode.setProperty("Initiator", pd.initiator);
	        	String[] activeOptions = {};
	        	processNode.setProperty("Active Options", activeOptions);
	       
	        	tx.success();
	        	tx.close();
	        }
	    	
	        System.out.println( "Process has been successfully created in PDG--> PDG Shutting down  ..." );
	        //System.out.println( "Actor is assigned to Activity --> PDG Shutting down  ..." );
			graphDb.shutdown();
	    }
//-------------------------------------------------------------------------------------	 
	    public void activityCreate(ActivityDef wid, activityState state)
	    {
	    	// START SNIPPET: startDb
		  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        
	        	// construct the Activity node
	        	Node activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Name", wid.name);
	        	activityNode.setProperty("ID", wid.activityID);
	        	activityNode.setProperty("State", state.name());
	        	activityNode.setProperty("Project", wid.projID);
	        	String[] activeOptions = {};
	        	activityNode.setProperty("Active Options", activeOptions);
	        	
	        	//find Project node and create an edge
	        	Node projNode = graphDb.findNode(PDG.Process, "ID", String.valueOf(wid.projID));
	        	if(projNode!=null)
	        	{
	        		Relationship rel1 = activityNode.createRelationshipTo(projNode, RelTypes.BELONG);
	        	}
	        	
	        	//find Actor node and create an edge
	        	Node actorNode = graphDb.findNode(PDG.Actor, "Name", wid.actor);
	        	if(actorNode!=null)
	        	{
	        		Relationship rel = actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	}
	        	tx.success();
	        }
	    	
	        System.out.println( "Activity has been successfully created in PDG--> PDG Shutting down  ..." );
	        System.out.println( "Actor is assigned to Activity --> PDG Shutting down  ..." );
			graphDb.shutdown();
	    }
//-------------------------------------------------------------------------------------
	    public void activityCreate1(ActivityDef wid, long ID, activityState state)
	    {
	    	// START SNIPPET: startDb
		  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        
	        	// construct the Activity node
	        	Node activityNode = graphDb.createNode(PDG.Activity);
	        	activityNode.setProperty("Name", wid.name);
	        	activityNode.setProperty("ID", ID);
	        	activityNode.setProperty("State", state.name());
	        	activityNode.setProperty("Project", wid.projID);
	        	String[] activeOptions = {};
	        	activityNode.setProperty("Active Options", activeOptions);
	        	
	        	//find Project node and create an edge
	        	Node projNode = graphDb.findNode(PDG.Process, "ID", String.valueOf(wid.projID));
	        	if(projNode!=null)
	        	{
	        		Relationship rel1 = activityNode.createRelationshipTo(projNode, RelTypes.BELONG);
	        	}
	        	
	        	
	        	tx.success();
	        }
	    	
	        System.out.println( "Activity has been successfully created in PDG--> PDG Shutting down  ..." );
	        System.out.println( "Actor is assigned to Activity --> PDG Shutting down  ..." );
			graphDb.shutdown();
	    }
//-------------------------------------------------------------------------------------
	    public void activityClaim(long activityID, String actor, activityState state)
	    {
	    	// START SNIPPET: startDb
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	      
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	Node activityNode = graphDb.findNode(PDG.Activity, "ID", activityID);
	        	if(activityNode==null){System.out.println( "node is null" );}
	        	
	        	//Create Actor nodes and edges
	        	Node actorNode = graphDb.findNode(PDG.Actor, "Name", actor);
	        	if(actorNode!=null)
	        	{
	        		Relationship rel = actorNode.createRelationshipTo(activityNode, RelTypes.PERFORM);
	        	}
	        	activityNode.setProperty("State", state.name());
	        	
	        	tx.success();
	        }
	    	
	        System.out.println( "Actor is assigned to Activity --> PDG Shutting down  ..." );
			graphDb.shutdown();
	       
	    }
//------------------------------------------------------------------------------------------------------	    
	    public void activityCreateOption(ActivityDef wid, String option)
	    {
	    	 graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
		     registerShutdownHook( graphDb );
	    	// START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	// add option to the active option List
		    	Node activityNode = graphDb.findNode(PDG.Activity, "ID", wid.activityID);
		    	Object val = activityNode.getProperty("Active Options");
		    	activityNode.setProperty("Active Options", appendArray((String[]) val, option));
		    	wid.activeOptionsList.add(option);
		    	
		    	// add option to the active option list of Project
		    	Node projNode = graphDb.findNode(PDG.Process, "ID",String.valueOf(wid.projID));
		    	projNode.setProperty("Active Options", appendArray((String[]) val, option));
		    	
		    	tx.success();
	        }
	        graphDb.shutdown();
	    }
//------------------------------------------------------------------------------------------------------	    
	    public String[] getActiveOptions(ActivityDef wid)
	    {
	    	Object val = new String[] {};
	    	graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
		     registerShutdownHook( graphDb );
	    	// START SNIPPET: transaction
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        	Node activityNode = graphDb.findNode(PDG.Activity, "ID", wid.activityID);
	        	val = activityNode.getProperty("Active Options");
	        	tx.success();
	        }
	        graphDb.shutdown();
	    	return (String[]) val;
	    }
//-------------------------------------------------------------------------------------	
	
	    public void taskCreate(TaskType task, long taskID, taskState state)
	    {
	    	// START SNIPPET: startDb
		  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        
	        	// construct the Task node
	        	Date date = new Date();
	        	System.out.println( "task name is : " + task.type);
	        	taskNode = graphDb.createNode(PDG.Task);
	        	taskNode.setProperty("Name", task.type);
	        	taskNode.setProperty("ID", taskID);
	        	taskNode.setProperty("State", state.name());
	        	taskNode.setProperty("Activity ID", task.activityID);
	        	taskNode.setProperty("Start", date.toString());
	        	taskNode.setProperty("End", "");
	        	taskNode.setProperty("Duration", task.duration);
	        	//create chile edge
	        	Node activityNode = graphDb.findNode(PDG.Activity, "ID", task.activityID);
	        	if(activityNode!=null)
	        	{
	        		Relationship rel = activityNode.createRelationshipTo(taskNode, RelTypes.CHILED);
	        	}
	        	
	        	//find Actor node and create an edge
	        	/*Node actorNode = graphDb.findNode(PDG.Actor, "Name", task.actor);
	        	if(actorNode!=null)
	        	{
	        		Relationship rel = actorNode.createRelationshipTo(taskNode, RelTypes.PERFORM);
	        	}*/
	        	tx.success();
	        }
	    	
	        System.out.println( "Task has been successfully created in PDG--> PDG Shutting down  ..." );
	        System.out.println( "ActorNode to TaskNode --> PDG Shutting down  ..." );
			graphDb.shutdown();
	    	
	    }
//----------------------------------------------------------------------------------------------------------------------------------
	    public void taskClaim(TaskType task, long taskID, taskState state)
	    {
	    	/*// START SNIPPET: startDb
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
	        registerShutdownHook( graphDb );
	      
	        try (Transaction tx = graphDb.beginTx() )
	        {
	        
	        	Node taskNode = graphDb.findNode(PDG.Task, "ID", taskID);
	        	
	        	
	        	//Create Actor nodes and edges
	        	Node actorNode = graphDb.findNode(PDG.Actor, "Name", task.actor);
	        	if(actorNode!=null)
	        	{
	        		Relationship rel = actorNode.createRelationshipTo(taskNode, RelTypes.PERFORM);
	        	}
	        	taskNode.setProperty("State", state.name());
	        	
	        	tx.success();
	        }
	    	
	        System.out.println( "ActorNode to TaskNode --> PDG Shutting down  ..." );
			graphDb.shutdown();*/
	    	
	    }

	    
//-----------------------------------------------------------------------------------------------------------------------------------
	   // public boolean verifyPreCondition(WorkItem workItem, WorkItemDef workDef, TaskType task)
	    public List<ArtifactType> verifyPreCondition(TaskType task)
		{

	    	boolean result = false;
			List<ArtifactType> NotOkinArtifact = new ArrayList<ArtifactType>();
			graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
			registerShutdownHook( graphDb );
			
			
			//   here we start to verify the transformation rules
	 
			try (Transaction tx = graphDb.beginTx())
			{
				Boolean check = true;
				// verify tr1 --> preconditions
				if(task.inArtifactList.size()==0)
				{
					result = true;
				}
				else
				{
					for (int i=0; i<task.inArtifactList.size(); i++)
					{
						// check artifact is single or set
						if(!task.inArtifactList.get(i).isCollection)
						{
							if(task.inArtifactList.get(i).usage.equals("T-S"))
							{
								verifySingleInArtifact(task.inArtifactList.get(i), NotOkinArtifact);
							}
							
						}
						else
						{
							verifyCollectionInArtifact(task.inArtifactList.get(i), NotOkinArtifact);
						}
					}
				}
				tx.success();
			  }
			System.out.println( "Execute Shutting down  ..." );
			graphDb.shutdown();
			
			if(NotOkinArtifact.isEmpty())
			{
				result = true;
				System.out.println( "All pre-conditiones are satisfied and you can start your work");	
			}
			
			return NotOkinArtifact;
		}


//-----------------------------------------------------------------------------------------------------------------------------------------

		public void taskStart(taskState state, TaskType task)
	    {
	    	
	    	// START SNIPPET: startDb
	  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try ( Transaction tx = graphDb.beginTx() )
	        {
	        
	        	// construct the Task node
	        	Node taskNode = graphDb.findNode(PDG.Task, "ID", task.taskID);
	        	taskNode.setProperty("State", state.name());
	        	// find input WP nodes and create Data edgs
	        	for (int i=0; i<task.inArtifactList.size(); i++)
	        	{
	        		if(!task.inArtifactList.get(i).isCollection )
					{
	        			if(!task.inArtifactList.get(i).instanceName.isEmpty())
	        			{
	        			Node wpNode = graphDb.findNode(PDG.Artifact, "Instance Name", task.inArtifactList.get(i).instanceName.get(0));
	        			
	        			Relationship rel = wpNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        			rel.setProperty("State", task.inArtifactList.get(i).state);
	        			rel.setProperty("Usage", task.inArtifactList.get(i).usage);
	        			if(task.inArtifactList.get(i).usage.equals("T-S")){rel.setProperty("Status", "ready");}
	        			else{rel.setProperty("Status", "waiting");}
	        			}
	        			}
	        		else
	        		{
	        			for(int j=0;  j<task.inArtifactList.get(i).instanceName.size(); j++)
	    				{
	        				wpNode = graphDb.findNode(PDG.Artifact, "Instance Name", task.inArtifactList.get(i).instanceName.get(j));
		        			Relationship rel = wpNode.createRelationshipTo(taskNode, RelTypes.DATA);
		        			rel.setProperty("State", task.inArtifactList.get(i).state);
		        			rel.setProperty("Usage", task.inArtifactList.get(i).usage);
		        			if(task.inArtifactList.get(i).usage.equals("T-S")){rel.setProperty("Status", "ready");}
		        			else{rel.setProperty("Status", "waiting");}
	    				}
	        		}
	        	}
	        	tx.success();
	        }
	    	
	        System.out.println( "WI has been successfully created in PDG--> PDG Shutting down  ..." );
			graphDb.shutdown();
	    }
//------------------------------------------------------------------------------------------------------------------------------		
		public void taskWaiting(taskState state, TaskType task, List<ArtifactType> notOKArtifact)
	    {
			// START SNIPPET: startDb
		  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try ( Transaction tx = graphDb.beginTx() )
	        {
	        	// construct the Task node
	        	taskNode = graphDb.findNode(PDG.Task, "ID", task.taskID);
	        	taskNode.setProperty("State", state.name());
	        	
	        	for (int i=0; i<notOKArtifact.size(); i++)
	        	{
	        		org.neo4j.graphdb.Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", notOKArtifact.get(i).instanceName.get(0));
	        		if (artifactNode==null)
	        		{
	        			/*
	        			 * THis part must be revised by Hanh Nhi/Christian
	        			 */
	        			artifactNode = graphDb.createNode(PDG.Artifact);
	    				artifactNode.setProperty("Name", notOKArtifact.get(i).type);
	        			artifactNode.setProperty("Instance Name", notOKArtifact.get(i).instanceName.get(0));
	        			artifactNode.setProperty("State", "Not Available");
	        			artifactNode.setProperty("Usage", notOKArtifact.get(i).usage);
	        		}
	        		
	        		Relationship rel = artifactNode.createRelationshipTo(taskNode, RelTypes.DATA);
	        		rel.setProperty("State", notOKArtifact.get(i).state);
	        		rel.setProperty("Usage", notOKArtifact.get(i).usage);
	        		rel.setProperty("Status", "waiting");
	        		
	        	}
	        	tx.success();
	        }
	    	
	        System.out.println( "Task is in the waiting state" );
			graphDb.shutdown();
			
	    }
//-----------------------------------------------------------------------------------------------------
		public void taskWakeUP(long taskID, taskState state)
	    {
	    	
	    	// START SNIPPET: startDb
	  	  
	        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	        registerShutdownHook( graphDb );
	       // IndexManager index = graphDb.index();
	        // END SNIPPET: startDb

	        // START SNIPPET: transaction
	        try ( Transaction tx = graphDb.beginTx() )
	        {
	        
	        	// construct the Task node
	        	Node taskNode = graphDb.findNode(PDG.Task, "ID", taskID);
	        	taskNode.setProperty("State", state.name());
	        	tx.success();
	        }
	    	
	        System.out.println( "WI has been successfully created in PDG--> PDG Shutting down  ..." );
			graphDb.shutdown();
	    }
//-----------------------------------------------------------------------------------------------------
		public List<ArtifactType> verifyPostCondition(long taskID, TaskType task)
		{
			boolean result = false;
			List<ArtifactType> NotOkInArtifact = new ArrayList<ArtifactType>();
			graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
			registerShutdownHook( graphDb );
			
			
			//   here we start to verify the transformation rules
	 
			try (Transaction tx = graphDb.beginTx())
			{
				org.neo4j.graphdb.Node taskNode = graphDb.findNode(PDG.Task, "ID", taskID);
				for(Relationship rel : taskNode.getRelationships(Direction.INCOMING, RelTypes.DATA))
	        	{
					if(rel.getProperty("Usage").equals("T-F"))
					{
						if(!rel.getProperty("State").equals(rel.getStartNode().getProperty("State")))
						{
							ArtifactType artifact = new ArtifactType();
							artifact.instanceName.add(rel.getStartNode().getProperty("Instance Name").toString());
							artifact.state = rel.getStartNode().getProperty("State").toString();
							artifact.usage = rel.getStartNode().getProperty("Usage").toString();
							artifact.type = rel.getStartNode().getProperty("Name").toString();
							NotOkInArtifact.add(artifact);
						}
					}
					
	        	}
				tx.success();
			  }
			System.out.println( "Execute Shutting down  ..." );
			graphDb.shutdown();
			
			if(NotOkInArtifact.isEmpty())
			{
				result = true;
				System.out.println( "All post-conditiones are satisfied and you can start your work");	
			}
			
			return NotOkInArtifact;
	}
// ----------------------------------------------------------------------------------------------------
		 public ArrayList<Long> uploadArtifact(ArtifactType artifact, long taskID)
		 {
			ArrayList<Long> wakeupTasks = new ArrayList<Long>(); 
			graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
			registerShutdownHook( graphDb );
			try (Transaction tx = graphDb.beginTx())
			{
				org.neo4j.graphdb.Node taskNode = graphDb.findNode(PDG.Task, "ID", taskID);
		    	Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", artifact.instanceName.get(0));
		    	
		    	produceSingleOutArtifact(artifactNode, taskNode,  artifact);
				wakeUpTasks(artifactNode, wakeupTasks);
				tx.success();
			}
		    
			graphDb.shutdown(); 
			return wakeupTasks;
		 }
		
//----------------------------------------------------------------------------------------------------
	    public void taskComplete(taskState state, TaskType task)
	    {
	    	graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
			registerShutdownHook( graphDb );
			
			ArrayList<Long> wakeupTasks = new ArrayList<Long>(); 
			try (Transaction tx = graphDb.beginTx())
			  {
				
				org.neo4j.graphdb.Node taskNode = graphDb.findNode(PDG.Task, "ID", task.taskID);
				if(taskNode != null)
				{
					// update state of task node
					Date date = new Date();
					taskNode.setProperty("State", state.name());
					taskNode.setProperty("End", date.toString());
		        	taskNode.setProperty("Duration", "");
					
				}	
				tx.success();
			  }
				System.out.println( "Execute Shutting down  ..." );
				graphDb.shutdown();
	    
	    }
//******** Private Methods ***********************************************************	    
	    private void verifySingleInArtifact(ArtifactType inArtifact, List<ArtifactType> NotOkinArtifact)
	    {

			org.neo4j.graphdb.Node node = graphDb.findNode(PDG.Artifact, "Instance Name", inArtifact.instanceName.get(0));
	
			if(node==null)
			{
				System.out.println( "Artifact " + inArtifact.type + "does not exist in the PDG" );
				ArtifactType preCondInArtifact = new ArtifactType();
				preCondInArtifact = inArtifact;
				NotOkinArtifact.add(preCondInArtifact);
			}
			else
			{
				System.out.println( "Input Artifact:" + inArtifact.instanceName.get(0) + 
					" exists in the PDG, Process Engine is verifying the State and Usage pre-conditions" );
				
				if(node.getProperty("State").equals(inArtifact.state))
				{
					System.out.println( "Input Artifact: "+ inArtifact.instanceName.get(0) + " is in a given state");
				}
				else 
				{
					System.out.println( "Input Artifact: "+ inArtifact.instanceName.get(0) + " is  not in a given state and we are veridying the USage");
					if (inArtifact.usage.equals("T-S")) 
					{
						ArtifactType preCondInArtifact = new ArtifactType();
						preCondInArtifact = inArtifact;
						NotOkinArtifact.add(preCondInArtifact);
					}
					else
					{
						System.out.println( "Input Artifact:" + inArtifact.instanceName.get(0) + 
								" has the usage type 'T-F' and Process Engine will verify its state on completing the task   " );
					}
						
				}
			}
		
	
			
			
		
	    }
	    
	    
//-----------------------------------------------------------------------------------------------------------------------------	    
	    private void verifyCollectionInArtifact(ArtifactType inArtifact, List<ArtifactType> NotOkinArtifact)
	    {
	    	
	    	for(int j=0;  j<inArtifact.instanceName.size(); j++)
			{
				org.neo4j.graphdb.Node node = graphDb.findNode(PDG.Artifact, "Instance Name", inArtifact.instanceName.get(j));
				if(node == null)
				{
					ArtifactType preCondInArtifact = new ArtifactType();
					preCondInArtifact.instanceName.add(inArtifact.instanceName.get(j));
					preCondInArtifact.state = inArtifact.state;
					NotOkinArtifact.add(preCondInArtifact);
					/*verifyResult.put(workDef.inArtifactList.get(i).instanceName.get(j), 
						"Input Artifact: " + workDef.inArtifactList.get(i).instanceName + " does not exists in the PDG" );*/
				}
				else
				{
					System.out.println( "Input Artifact:" + inArtifact.instanceName.get(j) + 
						" exists in the PDG, Process Engine is verifying the State pre-conditions" );
					
					if(node.getProperty("State").equals(inArtifact.state))
					{
						System.out.println( "Input Artifact: "+ inArtifact.instanceName.get(j) + " is in a given state");
						

					}
					else
					{
						if(node.getProperty("Usage").equals("T-S"))
						{
							ArtifactType preCondInArtifact = new ArtifactType();
							preCondInArtifact.instanceName.add(inArtifact.instanceName.get(j));
							preCondInArtifact.state = inArtifact.state;
							NotOkinArtifact.add(preCondInArtifact);
						}
						else
						{
							
						}
					
					}
				}
				
				
				
			}
			
		 }
//------------------------------------------------------------------------------------------------------------------------
	    public void produceSingleOutArtifact(Node artifactNode, Node taskNode, ArtifactType artifact)
	    {
	    	if(artifactNode==null)
				{
	    		System.out.println("Artifact Node does not exist in the PDG.. you must first upload it into DB");
					/*artifactNode = graphDb.createNode(PDG.Artifact);
					artifactNode.setProperty("Name", artifact.type);
        			artifactNode.setProperty("Instance Name", artifact.instanceName.get(0));
        			artifactNode.setProperty("State", artifact.state);
        			artifactNode.setProperty("Usage", artifact.usage);
        			Relationship rel = taskNode.createRelationshipTo(artifactNode, RelTypes.DATA);
        			rel.setProperty("State", artifact.state);
        			rel.setProperty("Usage", artifact.usage);
        			rel.setProperty("Status", "ready");*/
				}
				else if(!artifactNode.getProperty("Name").equals(artifact.type))
				{
					artifactNode = graphDb.createNode(PDG.Artifact);
					artifactNode.setProperty("Name", artifact.type);
	    			artifactNode.setProperty("Instance Name", artifact.instanceName.get(0));
	    			artifactNode.setProperty("State", artifact.state);
	    			artifactNode.setProperty("Usage", artifact.usage);
	    			Relationship rel = taskNode.createRelationshipTo(artifactNode, RelTypes.DATA);
	    			rel.setProperty("State", artifact.state);
	    			rel.setProperty("Usage", artifact.usage);
	    			rel.setProperty("Status", "ready");
				}
				else
				{
					artifactNode.setProperty("State", artifact.state);
	    			Relationship rel = taskNode.createRelationshipTo(artifactNode, RelTypes.DATA);
	    			rel.setProperty("State", artifact.state);
	    			rel.setProperty("Usage", artifact.usage);
	    			rel.setProperty("Status", "ready");
				}
				
			
	    }
//----------------------------------------------------------------------------------------------------------------------------	    
	    
	   public void wakeUpTasks(Node artifactNode, ArrayList<Long> wakeupTasks)
	   {
		   for(Relationship rel : artifactNode.getRelationships(Direction.OUTGOING, RelTypes.DATA))
       		{
			   if(rel.getProperty("Status").equals("waiting"))
			   {
				if(rel.getProperty("State").equals(artifactNode.getProperty("State")))
				{
					Boolean result = true;
					rel.setProperty("Status", "ready");
					Node waitingTaskNode = rel.getEndNode();
					for(Relationship rel1 : waitingTaskNode.getRelationships(Direction.INCOMING, RelTypes.DATA))
		        	{
						if(rel1.getProperty("Status").equals("waiting"))
						{
							result = false;
						}
		        	}
					if (result)
					{
						wakeupTasks.add((Long) waitingTaskNode.getProperty("ID"));
					}
					
				}}
				
       	}
	   }
//-------------------------------------------------------------------------------------------------------------------------------
	    public void produceCollectionOutArtifact(int i,TaskType task, ArrayList<Long> wakeupTasks, Node taskNode)
	    {
	    	for(int j=0;  j<task.outArtifactList.get(i).instanceName.size(); j++)
			{
				Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", task.outArtifactList.get(i).instanceName.get(j));
    			if(artifactNode==null)
    			{
    				artifactNode = graphDb.createNode(PDG.Artifact);
    				artifactNode.setProperty("Name", task.outArtifactList.get(i).type);
    				artifactNode.setProperty("Instance Name", task.outArtifactList.get(i).instanceName.get(j));
    				
    			}
    			artifactNode.setProperty("State", task.outArtifactList.get(i).state);
    			artifactNode.setProperty("Usage", task.outArtifactList.get(i).usage);
    			Relationship rel = taskNode.createRelationshipTo(artifactNode, RelTypes.DATA);
    			rel.setProperty("State", task.outArtifactList.get(i).state);
    			rel.setProperty("Usage", task.outArtifactList.get(i).usage);
    			rel.setProperty("Status", "ready");
			}
    			// here we wake up tasks are waiting for the produced artifact
    			
				
			
	    }
//------------------------------------------------------------------------------------------------------------------------
	   public Boolean verifyActivityPostCond(long activityID)
	   {
		   graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	       registerShutdownHook( graphDb );
		   Boolean result=true;
		   try (Transaction tx = graphDb.beginTx() )
	        {
			   Node activityNode = graphDb.findNode(PDG.Activity, "ID", activityID);
			   for(Relationship rel : activityNode.getRelationships(Direction.OUTGOING, RelTypes.CHILED))
	       		{
				   // verify IF all tasks are completed
				   if(!rel.getEndNode().getProperty("State").equals("completed"))
				   {
					   result = false;
				   }
	       		}
			   tx.success();
	        }
		   graphDb.shutdown();
		   return result;
		 
	   }
//------------------------------------------------------------------------------------------------------------------------	
	   public void activityComplete(long activityID, activityState state)
	   {
		   graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	       registerShutdownHook( graphDb );
		   try (Transaction tx = graphDb.beginTx() )
	        {
			   Node activityNode = graphDb.findNode(PDG.Activity, "ID", activityID);
			   activityNode.getProperty("State", state.name());
			   tx.success();
	        }
		   graphDb.shutdown();
	   }
//--------------------------------------------------------------------------------------------------------------------------
	   
	   public Boolean verifyProvokeEvent(long projID, String activity)
	   {
		   Boolean result = true;
		   graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	       registerShutdownHook( graphDb );
		   try (Transaction tx = graphDb.beginTx() )
	        {
			   Node ProjecNode = graphDb.findNode(PDG.Process, "ID", String.valueOf(projID));
			   for(Relationship rel : ProjecNode.getRelationships(Direction.INCOMING, RelTypes.BELONG))
			   {
				   if(rel.getStartNode().getProperty("Name").equals(activity))
				   {
					   result = false;
				   }
			   }
			   
			   tx.success();
	        }
		   graphDb.shutdown();
		   return result;
	   }
//------------------------------------------------------------------------------------------------------------------------	
   public  List<String> reloadInputArtifactValues(String artifactType)
   {
	   graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
       registerShutdownHook( graphDb );
       List<String> artifactList = new ArrayList<String>();
       	try (Transaction tx = graphDb.beginTx() )
	    {
       		ResourceIterator<Node> node = graphDb.findNodes(PDG.Artifact, "Name" , artifactType);
   			while(node.hasNext())
   			{
   				Node temp = node.next();
   				if(temp!=null)
   				{
   					artifactList.add(temp.getProperty("Instance Name").toString());
   				}
   			}
   		node.close();
   		tx.success();
	     }
		graphDb.shutdown();
	   return  artifactList;
   }	   
   
 //------------------------------------------------------------------------------------------------------------------------	
   public  List<String> reloadOutputArtifactValues(String artifactType)
   {
	   graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
       registerShutdownHook( graphDb );
       List<String> artifactList = new ArrayList<String>();
       	try (Transaction tx = graphDb.beginTx() )
	    {
       		ResourceIterator<Node> node = graphDb.findNodes(PDG.Artifact, "Name" , artifactType);
   			while(node.hasNext())
   			{
   				Node temp = node.next();
   				if(temp!=null)
   				{
   					artifactList.add(temp.getProperty("Instance Name").toString());
   				}
   			}
   		node.close();
   		tx.success();
	     }
		graphDb.shutdown();
	   return  artifactList;
   }	   
//--------------------------------------------------------------------------
   public ArrayList<Long> produceArtifact(ArtifactType artifact, long projID)
   {
   		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
   		registerShutdownHook( graphDb );
   		ArrayList<Long> wakeupTasks = new ArrayList<Long>(); 
       try (Transaction tx = graphDb.beginTx() )
       {
    	   
       	Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", artifact.instanceName.get(0));
       	if(artifactNode==null)
       	{
       		artifactNode = graphDb.createNode(PDG.Artifact);
       		artifactNode.setProperty("Name", artifact.type);
       		artifactNode.setProperty("Instance Name", artifact.instanceName.get(0));
       		artifactNode.setProperty("Usage", "T-S");
       	}
			artifactNode.setProperty("State", artifact.state);
			
		wakeUpTasks(artifactNode, wakeupTasks);
		
		Node projNode = graphDb.findNode(PDG.Process, "ID", String.valueOf(projID));
		artifact.usage = projNode.getProperty("Name").toString();
		tx.success();
			
       }
       
       
    System.out.println( "PDG  is Updated via DB Update Event" );
	graphDb.shutdown();
   
   	return wakeupTasks;
   	
   }
//****************** Change Evnet Handlers****************************************
   public ChangeRequest findChangeResponsible(ChangeRequest cr)
   {
	  graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	  registerShutdownHook( graphDb );
      try (Transaction tx = graphDb.beginTx() )
      {
   	   for(int i=0;i<cr.artifactList.size();i++)
   	   {
   		if(cr.artifactList.get(i).isArtifactChanged)
   		{
      	Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", cr.artifactList.get(i).instanceName.get(0));
      	if(artifactNode==null)
      	{
      		System.out.println( "PDG  Change handler cant find the Changed Artifact" );
      	}
      	else
      	{
      		for(Relationship rel : artifactNode.getRelationships(Direction.INCOMING, RelTypes.DATA))
        	{
      			if(cr.artifactList.get(i).isArtifactChanged)
      			{
					if(rel.getProperty("State").equals(cr.artifactList.get(i).state))
					{
						Node taskNode = rel.getStartNode();
						cr.resHres.actor=taskNode.getSingleRelationship(RelTypes.CHILED, Direction.INCOMING).getStartNode()
						.getSingleRelationship(RelTypes.PERFORM, Direction.INCOMING).getStartNode().getProperty("Name").toString();
						cr.resHres.role=taskNode.getSingleRelationship(RelTypes.CHILED, Direction.INCOMING).getStartNode()
								.getSingleRelationship(RelTypes.PERFORM, Direction.INCOMING).getStartNode().getProperty("Role").toString();
						
					}
				}
        	}
      	}}
   	   }
		tx.success();
			
      }
      
      
      //System.out.println( "PDG  is Updated via DB Update Event" );
      graphDb.shutdown();
	   
	   return cr;
   }
//------------------------------------------------------------------------------------------------
   public ChangeRequest findAffectedElements(ChangeRequest cr, ProcessEngineController controler)
   {
	  List<Long> tempList = new ArrayList<>(); 
	  cr.affectedArtifact = new ArrayList<>();
	  cr.affectedHres = new ArrayList<>();
	  graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	  registerShutdownHook( graphDb );
	  //here we found the Changed Artifact
	 
	  
      try (Transaction tx = graphDb.beginTx() )
      {
    	  Node taskNode = null;
    	  Node artifactNode = graphDb.findNode(PDG.Artifact, "Instance Name", cr.artifactList.get(0).instanceName.get(0));
    	  for (Relationship rell : artifactNode.getRelationships(RelTypes.DATA, Direction.INCOMING))
    	  {
    		  if (rell.getProperty("State").equals(artifactNode.getProperty("State")))
    		  {
    			  taskNode = rell.getStartNode();
    		  }
    	  }
    	  
    	  TraversalDescription impactGraph = graphDb.traversalDescription()
	        .depthFirst()
	        .relationships( RelTypes.DATA, Direction.OUTGOING )
	       // .relationships( RelTypes.PERFORM, Direction.INCOMING )
	        //.relationships( RelTypes.USED )
	        //.relationships( RelTypes.CHILED )
	       // .uniqueness( Uniqueness.NODE_GLOBAL )
	        .uniqueness( Uniqueness.RELATIONSHIP_GLOBAL );
      	
    	 /* for ( Relationship rel : impactGraph
      			.uniqueness( Uniqueness.RELATIONSHIP_GLOBAL )
      			.uniqueness( Uniqueness.NODE_GLOBAL )
      	        .traverse( artifactNode )
      	        .relationships() )
      		{
    		  if(rel.isType(RelTypes.PERFORM))
    		  {
    			  humanResourceType hres = new humanResourceType();
    			  hres.actor = rel.getStartNode().getProperty("Name").toString();
    			  hres.role = rel.getStartNode().getProperty("Role").toString();
    			  hres.tel = rel.getStartNode().getProperty("Telephone").toString();
    			  hres.email = rel.getStartNode().getProperty("Email").toString();
    			  cr.affectedHres.add(hres);
    		  }
      		}*/
    	  for ( Node node : impactGraph
        			.uniqueness( Uniqueness.RELATIONSHIP_GLOBAL )
        			.uniqueness( Uniqueness.NODE_GLOBAL )
        	        .traverse( taskNode )
        	        .nodes() )
        		{
    		  		if(node.hasLabel(PDG.Artifact))
    		  		{
    		  			// here we check all completed affected artifacts
    		  			for (Relationship rel: node.getRelationships(Direction.INCOMING, RelTypes.DATA))
    		  			{
    		  				if(!tempList.contains(rel.getId()))
    		  				{
    		  					tempList.add(rel.getId());
    		  					ArtifactType artifact = new ArtifactType();
    	    		  			artifact.type = (String) node.getProperty("Name");
    	    		  			artifact.state = (String) rel.getProperty("State");
    	    		  			artifact.instanceName.add(node.getProperty("Instance Name").toString());
    	    		  			artifact.task = rel.getStartNode().getProperty("Name").toString();
    	    		  			artifact.status = rel.getStartNode().getProperty("State").toString();
    	    		  			artifact.project = rel.getStartNode().getSingleRelationship(RelTypes.CHILED, Direction.INCOMING).getStartNode()
    	    		  					.getProperty("Project").toString();
    	    		  			Node actorNode = rel.getStartNode().getSingleRelationship(RelTypes.CHILED, Direction.INCOMING).getStartNode()
    	    	    					.getSingleRelationship(RelTypes.PERFORM, Direction.INCOMING).getStartNode();
    	    		  			artifact.actor = actorNode.getProperty("Name").toString();
    	    		  			
    	    		  			humanResourceType hres = new humanResourceType();
    	    		  			hres.actor = actorNode.getProperty("Name").toString();
    	    		  			hres.role = actorNode.getProperty("Role").toString();
    	    		  			hres.tel = actorNode.getProperty("Telephone").toString();
    	    		  			hres.email = actorNode.getProperty("Email").toString();
    	    		  			cr.affectedHres.add(hres);
    	    		  			
    	    		  			cr.affectedArtifact.add(artifact);
    		  				}
    		  			}
    		  			// here we check all inProgress affected artifacts
    		  			for (Relationship rel: node.getRelationships(Direction.OUTGOING, RelTypes.DATA))
    		  			{
    		  				//if(!tempList.contains(rel.getId()))
    		  				{
    		  				//	tempList.add(rel.getId());
    		  					
    		  					if(rel.getEndNode().getProperty("State").equals("inProgress"))
    		  					{
    		  						TaskType task = controler.claimedTaskWorkItemss.get(rel.getEndNode().getProperty("ID"));
    		  						List<String> tempLis = new ArrayList<>();
    		  						for (Relationship rel1: rel.getEndNode().getRelationships(Direction.OUTGOING, RelTypes.DATA))
    		    		  			{
    		  							if(rel1!=null)
    		  							{
    		  								tempLis.add(rel1.getEndNode().getProperty("Name").toString());
    		  							}
    		    		  			}
    		  							
		    		  				for (int i=0; i<task.outArtifactList.size(); i++)
		    		  				{
		    		  					if(!tempLis.contains(task.outArtifactList.get(i).type))
		    		  					{
    		  								ArtifactType artifact = new ArtifactType();
    		  								artifact = task.outArtifactList.get(i);
    		  								Node actorNode = rel.getEndNode().getSingleRelationship(RelTypes.CHILED, Direction.INCOMING).getStartNode()
    		    	    	    					.getSingleRelationship(RelTypes.PERFORM, Direction.INCOMING).getStartNode();
    		    	    		  			artifact.actor = actorNode.getProperty("Name").toString();
    		    	    		  			artifact.task = rel.getEndNode().getProperty("Name").toString();
    		    	    		  			artifact.status = rel.getEndNode().getProperty("State").toString();
    		    	    		  			artifact.project = rel.getEndNode().getSingleRelationship(RelTypes.CHILED, Direction.INCOMING).getStartNode()
    		    	    		  					.getProperty("Project").toString();
    		    	    		  			
    		    	    		  			humanResourceType hres = new humanResourceType();
    		    	    		  			hres.actor = actorNode.getProperty("Name").toString();
    		    	    		  			hres.role = actorNode.getProperty("Role").toString();
    		    	    		  			hres.tel = actorNode.getProperty("Telephone").toString();
    		    	    		  			hres.email = actorNode.getProperty("Email").toString();
    		    	    		  			cr.affectedHres.add(hres);
    		    	    		  			
    		    	    		  			cr.affectedArtifact.add(artifact);
		    		  					}
		    		  				}
    		  					}
  								
	    		  			
    		  							
    		  					
    	    		  			
    		  				}
    		  			}
    		  			
    		  		}
    		  		/*if(node.hasLabel(PDG.Actor))
    		  		{
    		  			humanResourceType hres = new humanResourceType();
    		  			hres.actor = node.getProperty("Name").toString();
    		  			hres.role = node.getProperty("Role").toString();
    		  			hres.tel = node.getProperty("Telephone").toString();
    		  			hres.email = node.getProperty("Email").toString();
    		  			cr.affectedHres.add(hres);
    		  		}*/
        		}
    	  
		tx.success();
			
      }
      
      
      System.out.println( "PDG  Affected Artifacts & Actors Are Found" );
      graphDb.shutdown();
	   
	   return cr;
   }
//***********************Process Monitoring*******************************
   
   public List<ProcessDef> getProcessInstances()
   {
	   List<ProcessDef>  pdList = new ArrayList<ProcessDef>();
	   graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
	   registerShutdownHook( graphDb );
	   ArrayList<Long> wakeupTasks = new ArrayList<Long>(); 
	   ProcessDef pd = null;
	   try (Transaction tx = graphDb.beginTx() )
	   {
		   ResourceIterator<Node> nodes = graphDb.findNodes(PDG.Process);
		   while (nodes.hasNext()) 
		   {
			  
			   Node node = nodes.next();
			   if(node!=null)
			   {
				   System.out.println(node.getId() + "salam");
				   System.out.println(node.getProperty("Name").toString() + "salam");
				   
			  // pd.id = node.getProperty("ID").toString(); 
			  // pd.name = node.getProperty("Name").toString(); 
			   //pdList.add(pd);
		   }}
			   tx.success();
			
      }
      
      
   System.out.println( "PDG  is Updated via DB Update Event" );
	graphDb.shutdown();	
	   
	   
	   return pdList;
	   
   }	    
   public List<MonitType> renderMonitoringUI(String processId)
 	{
   	List<MonitType> mtList = new ArrayList<MonitType>();
   	
   	graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(PDG_PATH);
       registerShutdownHook( graphDb );
       try (Transaction tx = graphDb.beginTx())
	    {
       	Node node = graphDb.findNode(PDG.Process, "ID" , processId);
       	System.out.println(node.getProperty("Name"));
       	for (Relationship rel : node.getRelationships(RelTypes.BELONG, Direction.INCOMING))
     	  	{
       		for (Relationship rell : rel.getStartNode().getRelationships(RelTypes.CHILED, Direction.OUTGOING))
 	      	  	{
       				MonitType mt = new MonitType();
       				mt.type = "task";
 	      		  	mt.activityID = rel.getStartNode().getProperty("ID").toString();
 	      		  	mt.activityName = rel.getStartNode().getProperty("Name").toString();
 	      		  	mt.id = rell.getEndNode().getProperty("ID").toString();
 	      		  	mt.name = rell.getEndNode().getProperty("Name").toString();
 	      		  	mt.actor = rel.getStartNode().getSingleRelationship(RelTypes.PERFORM, Direction.INCOMING).getStartNode().getProperty("Name").toString(); 
 	      		  	mt.state = rell.getEndNode().getProperty("State").toString();
 	      		  	mt.start = rell.getEndNode().getProperty("Start").toString();
 	      		  	mt.end = rell.getEndNode().getProperty("End").toString();
 	      		  	mt.duration = rell.getEndNode().getProperty("Duration").toString();
 	      		  	
 	      		  	mt.mta.actor = rel.getStartNode().getSingleRelationship(RelTypes.PERFORM, Direction.INCOMING).getStartNode().getProperty("Name").toString();
 	      		  	if(rell.getEndNode().getSingleRelationship(RelTypes.DATA, Direction.OUTGOING)!=null)
 	      		  	{
	 	      		  	mt.mta.id = rell.getEndNode().getSingleRelationship(RelTypes.DATA, Direction.OUTGOING).getEndNode().getProperty("Instance Name").toString(); 
	 	      		  	mt.mta.name = rell.getEndNode().getSingleRelationship(RelTypes.DATA, Direction.OUTGOING).getEndNode().getProperty("Name").toString();
	 	      		  	mt.mta.state = rell.getEndNode().getSingleRelationship(RelTypes.DATA, Direction.OUTGOING).getEndNode().getProperty("State").toString();
	 	      		  	if(mt.mta.state.equals("defined") ||mt.mta.state.equals("outlined") )
	 	      		  	{
	 	      		  		mt.mta.cp ="100%"; 
	 	      		  	}
	 	      		  	else
	 	      		  	{
	 	      		  		mt.mta.cp = String.valueOf(ThreadLocalRandom.current().nextInt(1, 98))+"%";
	 	      		  	}
	 	      		 }
 	      		  	mtList.add(mt);
 	      	  	}
     		  
     	  	 }
   		tx.success();
	     }
		graphDb.shutdown();
 		return mtList;
 		
 	}
   
//*******CLEAR DB *******************************************************
	    
	    public void clearDb()
	    {
	        try
	        {
	            FileUtils.deleteRecursively( new File( "target/PDG" ) );
	        }
	        catch ( IOException e )
	        {
	            throw new RuntimeException( e );
	        }
	    }
	    	    
//*********************************************************************
	    private String[] appendArray(String[] array, String x)
	    {
	        String[] result = new String[array.length + 1];

	        for(int i = 0; i < array.length; i++)
	            result[i] = array[i];

	        result[result.length - 1] = x;

	        return result;
	    }
	    
	    
	    
	    
//*********************REMOVE DB ******************************************
	    
//	    void removeData()
//	    {
//	        try ( Transaction tx = graphDb.beginTx() )
//	        {
//	            // START SNIPPET: removingData
//	            // let's remove the data
//	            firstNode.getSingleRelationship( RelTypes.DATA, Direction.OUTGOING ).delete();
//	            firstNode.delete();
//	            secondNode.delete();
//	            // END SNIPPET: removingData
//
//	            tx.success();
//	        }
//	    }
//------------------------------------------------------------------------------------------

//******************** SHUT DOWN DB **************************************************
	    
	    void shutDown()
	    {
	        System.out.println("Schema is Created");
	        System.out.println( "Shutting down database ..." );
	        // START SNIPPET: shutdownServer
	        graphDb.shutdown();
	        // END SNIPPET: shutdownServer
	    }

	    // START SNIPPET: shutdownHook
	    static void registerShutdownHook( final GraphDatabaseService graphDb )
	    {
	        // Registers a shutdown hook for the Neo4j instance so that it
	        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
	        // running application).
	        Runtime.getRuntime().addShutdownHook( new Thread()
	        {
	            @Override
	            public void run()
	            {
	                graphDb.shutdown();
	            }
	        } );
	    }
	    
	    // END SNIPPET: shutdownHook


	}
	

