package com.bape.event;

import javax.swing.JButton;



import com.bape.type.Activity;
import com.bape.type.ActivityDef;

public class ProcessAbortEvent extends JButton 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Activity activity;
	ActivityDef wid;
	long projID;
	
	public ProcessAbortEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}
	
	
	public Activity getActivity()
	{
		return activity;
	}
	
	public void setWID(ActivityDef wid)
	{
		this.wid = wid;
	}
	
	
	public ActivityDef getWID()
	{
		return wid;
	}
	
	public void setProjID(long projID)
	{
		this.projID = projID;
	}
	public long getProjID()
	{
		return projID;
	}
	
	public void setID(long activityID)
	{
		activity.id = activityID;
	}
	public long getID()
	{
		return activity.id;
	}
	
	public void setRole(String role)
	{
		this.activity.role = role;
	}
	public void setActor(String actor)
	{
		this.activity.actor = actor;
	}
	
	public String getRole()
	{
		return activity.role;
	}
	public String getActor()
	{
		return activity.actor;
	}
}
