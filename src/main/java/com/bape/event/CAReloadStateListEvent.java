package com.bape.event;
import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAReloadStateListEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String artifact;
	ActivityModellingView amv;
	public CAReloadStateListEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setArtifact(String artifact)
	{
		this.artifact = artifact;
	}
	
	public String getArtifact()
	{
		return artifact;
	}
	/*
	public void setProcess(String process)
	{
		this.process = process;
	}

	public String getProcess()
	{
		return process;
	}
	*/
	public void setActivityModelinView(ActivityModellingView amv)
	{
		this.amv = amv;
	}
	
	public ActivityModellingView getActivityModelinView()
	{
		return amv;
	}
}
