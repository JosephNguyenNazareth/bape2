package com.bape.event;



import com.bape.type.ActivityDef;

public class ActivityProvokeEvent 
{
	private static final long serialVersionUID = 1L;
	String  actor, role;
	ActivityDef wid;
	
	public ActivityProvokeEvent()
	{
		//this.setText(name);
		
	}
	
	public void setWID(ActivityDef wid)
	{
		this.wid = wid;
	}
	
	
	public ActivityDef getWID()
	{
		return wid;
	}
	
	public void setProjID(long projID)
	{
		this.wid.projID = projID;
	}
	public long getProjID()
	{
		return wid.projID;
	}
	public void setID(long activityID)
	{
		this.wid.activityID = activityID;
	}
	public long getID()
	{
		return wid.activityID;
	}
	
	public void setRole(String role)
	{
		this.wid.role = role;
	}
	public void setActor(String actor)
	{
		this.wid.actor = actor;
	}
	
	public String getRole()
	{
		return role;
	}
	public String getActor()
	{
		return actor;
	}

}
