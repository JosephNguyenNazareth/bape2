package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ArtifactType;

public class ArtifactUploadEvent extends JButton
{
	ArtifactType artifact;
	long taskID;
	public ArtifactUploadEvent(String name)
	{
		this.setText(name);
		
	}

	public void setArtifact(ArtifactType artifact)
	{
		this.artifact = artifact;
	
	}
		
	public ArtifactType getArtifact()
	{
		return this.artifact;
	}

	public void setWI(long taskID) 
	{
		this.taskID = taskID;
		
	}
	public long getWI()
	{
		return this.taskID;
	}
}
