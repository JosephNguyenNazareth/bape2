package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class PFUploadActivityEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process;
	List<Object> activityList;
	ActivityModellingView amv;
	
	public PFUploadActivityEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}

	public void setActivityList(List<Object> activityList)
	{
		this.activityList = activityList;
	}
	
	public List<Object>  getActivityList()
	{
		return this.activityList;
	}
	
	public void setActivityModelinView(ActivityModellingView amv)
	{
		this.amv = amv;
	}
	
	public ActivityModellingView getActivityModelinView()
	{
		return amv;
	}
}
