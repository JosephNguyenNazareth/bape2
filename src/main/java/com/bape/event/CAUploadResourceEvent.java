package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAUploadResourceEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String  process;
	List<Object> roleList;
	public CAUploadResourceEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	
	
	public void setRoleList(List<Object> roleList)
	{
		this.roleList = roleList;
	}
	
	public List<Object>  getRoleList()
	{
		return this.roleList;
	}
}
