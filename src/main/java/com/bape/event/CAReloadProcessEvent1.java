package com.bape.event;
import javax.swing.JButton;

import com.bape.view.ActivityListView;
import com.bape.view.ActivityModellingView;

public class CAReloadProcessEvent1 extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role;
	ActivityListView alv;
	public CAReloadProcessEvent1(String name)
	{
		this.setText(name);
		
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	

	public void setActivityListView(ActivityListView alv)
	{
		this.alv = alv;
	}
	
	public ActivityListView getActivityListView()
	{
		return alv;
	}
}
