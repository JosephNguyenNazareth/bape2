package com.bape.event;

import javax.swing.JButton;

import com.bape.type.ChangeRequest;

public class ChangeImpactAnalysisEvent  extends JButton
{
	ChangeRequest cr;
	public ChangeImpactAnalysisEvent(String name)
	{
		this.setText(name);
		
	}

	public void setChangeRequest(ChangeRequest cr)
	{
		this.cr = cr;
	}
	
	public ChangeRequest getChangeRequest()
	{
		return cr;
	}
}
