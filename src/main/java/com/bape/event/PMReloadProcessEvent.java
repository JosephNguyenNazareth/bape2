package com.bape.event;
import javax.swing.JButton;

import com.bape.view.ActivityModellingView;
import com.bape.view.ProcessMonitoringView;

public class PMReloadProcessEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String id;
	ProcessMonitoringView pmv;
	public PMReloadProcessEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setProcessMonitoringView(ProcessMonitoringView pmv)
	{
		this.pmv = pmv;
	}
	
	public ProcessMonitoringView getProcessMonitoringView()
	{
		return pmv;
	}
}
