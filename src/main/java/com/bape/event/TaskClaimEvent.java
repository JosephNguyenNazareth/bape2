package com.bape.event;

import javax.swing.JButton;



import com.bape.type.ActivityDefWrapper;
import com.bape.type.TaskType;

public class TaskClaimEvent extends JButton
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String viewID;
	TaskType task;
	ActivityDefWrapper wid;
	String actor;

	public TaskClaimEvent(String name)
	{
		this.setText(name);
		
	}
	
	
	public void setTask(TaskType task)
	{
		this.task = task;
	}
	
	
	public TaskType getTask()
	{
		return task;
	}
	
	public void setRole(String role)
	{
		this.task.role = role;
	}
	public void setActor(String actor)
	{
		this.actor = actor;
	}
	
	public String getActor()
	{
		return this.actor;
	}
}
