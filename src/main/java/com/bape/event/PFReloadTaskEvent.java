package com.bape.event;
import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class PFReloadTaskEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, activity, process;
	ActivityModellingView amv;
	public PFReloadTaskEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}

	public String getProcess()
	{
		return process;
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}

	public String getRole()
	{
		return role;
	}
	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getActivity()
	{
		return activity;
	}
	
	public void setActivityModelinView(ActivityModellingView amv)
	{
		this.amv = amv;
	}
	
	public ActivityModellingView getActivityModelinView()
	{
		return amv;
	}
}
