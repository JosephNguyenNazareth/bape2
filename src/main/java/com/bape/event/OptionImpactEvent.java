package com.bape.event;

import javax.swing.JButton;



import com.bape.type.ActivityDefWrapper;
import com.bape.type.TaskType;

public class OptionImpactEvent extends JButton
{
	private static final long serialVersionUID = 1L;
	String viewID, option;
	ActivityDefWrapper wid;
	TaskType task;
	
	
	public OptionImpactEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setWID(ActivityDefWrapper wid)
	{
		this.wid = wid;
	}
	
	
	public ActivityDefWrapper getWID()
	{
		return wid;
	}
	
	public void setTask(TaskType task)
	{
		this.task = task;
	}
	
	
	public TaskType getTask()
	{
		return this.task;
	}
	
	public void setOption(String option)
	{
		this.option = option;
	}
	
	
	public String getOption()
	{
		return this.option;
	}
	
	
}
