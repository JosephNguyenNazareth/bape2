package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class PFDeleteArtifactEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process, activity, task, artifact, state;
	public PFDeleteArtifactEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	
	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getActivity()
	{
		return activity;
	}
	
	public void setTask(String task)
	{
		this.task = task;
	}

	public String getTask()
	{
		return task;
	}
	
	public void setArtifact(String artifact)
	{
		this.artifact = artifact;
	}
	
	public String  getArtifact()
	{
		return this.artifact;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	
	public String  getState()
	{
		return this.state;
	}
}
