package com.bape.event;

import javax.swing.JButton;



import com.bape.type.Activity;
import com.bape.type.ActivityDef;
import com.bape.type.ProcessDef;

public class ProcessCreateEvent extends JButton 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Activity activity;
	long wid, version;
	public ProcessDef pd;
	//long projID;
	
	public ProcessCreateEvent(String name)
	{
		this.setText(name);
		
	}
	

	
	public void setWID(ProcessDef pd)
	{
		
		this.pd = pd;
	}
	
	
	public ProcessDef getWID()
	{
		return pd;
	}
}
