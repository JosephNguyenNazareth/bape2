package com.bape.event;
import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAReloadArtifactEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process;
	ActivityModellingView amv;
	public CAReloadArtifactEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}

	public String getProcess()
	{
		return process;
	}
	
	public void setActivityModelinView(ActivityModellingView amv)
	{
		this.amv = amv;
	}
	
	public ActivityModellingView getActivityModelinView()
	{
		return amv;
	}
}
