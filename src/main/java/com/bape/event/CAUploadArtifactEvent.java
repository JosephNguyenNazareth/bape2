package com.bape.event;
import java.util.List;

import javax.swing.JButton;

import com.bape.view.ActivityModellingView;

public class CAUploadArtifactEvent extends JButton
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String role, process;
	List<Object> artifactList;
	public CAUploadArtifactEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setProcess(String process)
	{
		this.process = process;
	}
	
	public String getProcess()
	{
		return process;
	}
	public void setRole(String role)
	{
		this.role = role;
	}
	
	public String getRole()
	{
		return role;
	}
	
	public void setArtifactList(List<Object> artifactList)
	{
		this.artifactList = artifactList;
	}
	
	public List<Object>  getArtifactList()
	{
		return this.artifactList;
	}
}
