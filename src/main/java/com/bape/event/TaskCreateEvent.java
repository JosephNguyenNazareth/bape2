package com.bape.event;
import javax.swing.JButton;



import com.bape.type.TaskType;
import com.bape.type.ActivityDefWrapper;
public class TaskCreateEvent extends JButton
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String viewID;
	TaskType task;
	long taskID, activityID;
	String actor;
	
	public TaskCreateEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setTask(TaskType task)
	{
		this.task = task;
	}
	
	
	public TaskType getTask()
	{
		return task;
	}
	
	public void setID(long taskID)
	{
		this.task.taskID = taskID;
	}
	public void setActivityID(long activityID)
	{
		this.activityID = activityID;
	}
	public long getID()
	{
		return task.taskID;
	}
	
	public void setRole(String role)
	{
		this.task.role = role;
	}
	public void setActor(String actor)
	{
		this.actor = actor;
	}
	
	public String getActor()
	{
		return this.actor;
	}
}
