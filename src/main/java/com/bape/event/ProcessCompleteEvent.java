package com.bape.event;

import javax.swing.JButton;



import com.bape.type.ActivityDefWrapper;

public class ProcessCompleteEvent extends JButton
{
	ActivityDefWrapper wid;
	long id;
	public ProcessCompleteEvent(String name)
	{
		this.setText(name);
		
	}
	
	public void setWI(long id)
	{
		this.id = id;
	
	}
	
	
	public long getWI()
	{
		return this.id;
	}
	
	public void setWID(ActivityDefWrapper wid)
	{
		this.wid = wid;
	
	}
	
	
	public ActivityDefWrapper getWID()
	{
		return this.wid;
	}
}
