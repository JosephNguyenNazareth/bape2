package com.bape.controler;

import java.util.HashMap;
import java.util.List;

import com.bape.model.DBModel;
import com.bape.model.OrganizationalModel;
import com.bape.model.PDGModel;
import com.bape.type.humanResourceType;
import com.bape.view.ActivityListView;
import com.bape.view.ActivityModellingView;
import com.bape.view.ChangeManagerView;
import com.bape.view.DBView;
import com.bape.view.ProcessActorUI;
import com.bape.view.ProcessMonitoringView;
import com.bape.view.ProcessOwnerUI;

public class ProcessMain 
{
	public static void main(String[] args) 
	{
		HashMap<String, ActivityListView> activityViewMap = new HashMap<String, ActivityListView>();
		//ProcessMonitoringView pmv = null;
		/**
		 * Initialize the Process Engine (Controller) 
		 */
		ProcessEngineController controler = new ProcessEngineController(activityViewMap);
		
		/**
		 * Initialise the Organizational Model
		 * 
		 */
		DBModel dbModel = DBModel.getInstance();
		dbModel.clearDb();
		dbModel.initilizeDB();
		
		OrganizationalModel orgModel = OrganizationalModel.getInstance();
		List<humanResourceType> hresList = orgModel.createLogIn();
		
		PDGModel pdg = PDGModel.getInstance();
		pdg.clearDb();
		pdg.initilizePDG();
		pdg.createActor(hresList);
		
		/**
		 * initialize the View
		 */
		//OrganizationalView orgView = new OrganizationalView(controler);
		
		
		//DBView dbV = new DBView(controler);
		for (int i =0 ; i<hresList.size(); i++)
		{
			if(hresList.get(i).role.equals("Change Manager"))
			{
				ChangeManagerView cmv = new ChangeManagerView(controler, hresList.get(i));
				controler.cmv = cmv;
			}
			else if(hresList.get(i).role.equals("Project Manager"))
			{
				ProcessOwnerUI poui = new ProcessOwnerUI(controler, hresList.get(i), dbModel.getProjIDs(), activityViewMap);
			}
			else 
			{
				ProcessActorUI paui = new ProcessActorUI(controler, hresList.get(i), dbModel.getProjIDs(), activityViewMap);
			}
		}
		
		
	}
}