package com.bape.controler;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

import org.w3c.dom.NodeList;

import com.bape.event.ActivityClaimEvent;
import com.bape.event.ActivityCompleteEvent;
import com.bape.event.ActivityCreateEvent;
import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.ActivityEvent;
import com.bape.event.ActivityProvokeEvent;
import com.bape.event.ArtifactSelectEvent;
import com.bape.event.ArtifactUploadEvent;
import com.bape.event.CADeleteArtifactEvent;
import com.bape.event.CADeleteOptionEvent;
import com.bape.event.CADeleteProcessEvent;
import com.bape.event.CAReloadArtifactEvent;
import com.bape.event.CAReloadArtifactListEvent;
import com.bape.event.CAReloadOptionEvent;
import com.bape.event.CAReloadProcessEvent;
import com.bape.event.CAReloadProcessEvent1;
import com.bape.event.CAReloadRoleEvent;
import com.bape.event.CADeleteRoleeEvent;
import com.bape.event.CAReloadStateEvent;
import com.bape.event.CAReloadStateForActTabEvent;
import com.bape.event.CAReloadStateListEvent;
import com.bape.event.CAUploadArtifactEvent;
import com.bape.event.CAUploadOptionEvent;
import com.bape.event.CAUploadProcessEvent;
import com.bape.event.CAUploadRoleEvent;
import com.bape.event.CAUploadStateEvent;
import com.bape.event.ChangeEvaluateRequest;
import com.bape.event.ChangeImpactAnalysisEvent;
import com.bape.event.ChangeInformActorsEvent;
import com.bape.event.ChangeRequestActionEvent;
import com.bape.event.ChangeRequestEvent;
import com.bape.event.DBUpdateEvent;
import com.bape.event.PFDeleteActivityEvent;
import com.bape.event.PFDeleteArtifactEvent;
import com.bape.event.PFDeleteTaskEvent;
import com.bape.event.PFReloadActivityEvent;
import com.bape.event.PFReloadArtifactEvent;
import com.bape.event.PFReloadTaskEvent;
import com.bape.event.PFUploadActivityEvent;
import com.bape.event.PFUploadArtifactEvent;
import com.bape.event.PFUploadTaskEvent;
import com.bape.event.PMReloadProcessEvent;
import com.bape.event.ProcessCreateEvent;
import com.bape.event.SendChangeRequestEvent;
import com.bape.event.SendChangeRequestResEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskCompleteEvent;
import com.bape.event.TaskCreateEvent;
import com.bape.event.TaskStartEvent;
import com.bape.model.ArtifactStateMachine.artifactState;
import com.bape.model.CompanyAssetModel;
import com.bape.model.OrganizationalModel;
import com.bape.model.PDGModel;
import com.bape.model.ProcessFragmentModel;
import com.bape.model.TaskStateMachine;
import com.bape.model.TaskStateMachine.taskState;
import com.bape.model.TaskWakeUpEvent;
import com.bape.model.WellFormedNessRules;
import com.bape.model.ActivityStateMachine.activityState;
import com.bape.type.ActivityDef;
import com.bape.type.ActivityInsWrapper;
import com.bape.type.ArtifactType;
import com.bape.type.ArtifactWrapper;
import com.bape.type.ChangeRequest;
import com.bape.type.MonitType;
import com.bape.type.ProcessDef;
import com.bape.type.ProvokeDef;
import com.bape.type.ProvokeType;
import com.bape.type.TaskInsWrapper;
import com.bape.type.TaskType;
import com.bape.type.TaskType.TYPE;
import com.bape.type.generateXML;
import com.bape.view.ActivityListView;
import com.bape.view.ChangeManagerView;
import com.bape.view.ProcessMonitoringView;
import com.bape.view.TaskInstanceExecutionPanel;
import com.bape.view.TaskPanel;

public class ProcessEngineController implements  ActionListener
{

	

	/**
	 *  Model 
	 */
	// Process Model
	public ProcessFragmentModel PFModel;
	//Company Assets Model
	public CompanyAssetModel CAModel;
	//Oraganizational model
	public OrganizationalModel orgModel;
	// PDG Model
	public PDGModel pdgModel;
	//task SM model
	public TaskStateMachine tsmModel;
	//WellFormedNess Rules
	public WellFormedNessRules wfnr;
	/**
	 * View
	 */
	List<ActivityListView> taskDefViewList;
	HashMap<String, ActivityListView> activityViewMap;
	// later mus be modified.. as the owner view (TaskInstanceView) is the responsible of instanciating the class TaskInstanceExecutionView
	HashMap<Long, TaskInstanceExecutionPanel> taskInstanceExecutionViewMap = new HashMap<Long, TaskInstanceExecutionPanel>();
	HashMap<Long, TaskPanel> taskPanelViewMap = new HashMap<Long, TaskPanel>();
	HashMap<Long, TaskInstanceExecutionPanel> taskExePanelViewMap = new HashMap<Long, TaskInstanceExecutionPanel>();

	
	/**
	 *Variables
	 */
	static long ID=0; 
	static long ProcessId=0;
	String InstanceID;
	
	public Map<Long, ActivityDef> ActWorkItemss = new HashMap<Long, ActivityDef>();
	public Map<Long, ActivityDef> claimedActWorkItemss = new HashMap<Long, ActivityDef>();
	public Map<Long, TaskType> claimedTaskWorkItemss = new HashMap<Long, TaskType>();
	public Map<Long, TaskType> waitingTaskWorkItemss = new HashMap<Long, TaskType>();
	
	List<ProvokeDef> provokerList = new ArrayList<ProvokeDef>();
	public ProcessMonitoringView pmv;
	ChangeManagerView cmv;
	//Constructor 
	public ProcessEngineController(HashMap<String, ActivityListView> activityViewMap)
	{
		
		//view
		this.activityViewMap = activityViewMap;
		this.pmv = pmv;
		//model
		this.PFModel = new ProcessFragmentModel();
		this.CAModel = new CompanyAssetModel();
		this.orgModel = OrganizationalModel.getInstance();
		this.pdgModel = PDGModel.getInstance();
	//	this.tsmModel = new TaskStateMachine();
		this.wfnr = new WellFormedNessRules();
		
	}
	
	// Event Listener
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		
		Object event = (Object) e.getSource();
		/**
		 * Process Fragment Events
		 */
		if(event instanceof CAReloadProcessEvent)
		{	
			CAReloadProcessEvent carp = (CAReloadProcessEvent) event;
			carp.getActivityModelinView().reloadProcessList(CAModel.reloadProcessList(carp.getActivityModelinView().hres));
			carp.getActivityModelinView().reloadRoleInfo(CAModel.reloadRoleList(carp.getActivityModelinView().hres));
			//pfre.getActivityModelinView().reloadArtifactList(CAModel.reloadArtifactList());
			//carp.getActivityListView().reloadProcessList(CAModel.reloadProcessList(carp.getActivityModelinView().hres));
		}
		if(event instanceof CAReloadProcessEvent1)
		{	
			CAReloadProcessEvent1 carp = (CAReloadProcessEvent1) event;
			
			carp.getActivityListView().reloadProcessList(CAModel.reloadProcessList(carp.getActivityListView().hres));
		}
		else if(event instanceof CAUploadProcessEvent)
		{
			CAUploadProcessEvent pfup = (CAUploadProcessEvent) event;
			CAModel.uploadProcessList(pfup.getProcessList());
		}
		else if(event instanceof CADeleteProcessEvent)
		{
			CADeleteProcessEvent cadp = (CADeleteProcessEvent) event;
			CAModel.deleteProcess(cadp.getProcess());
		}
		else if(event instanceof CAReloadRoleEvent)
		{	//just show the role List to the Project Manager
			CAReloadRoleEvent carr = (CAReloadRoleEvent) event;
			if(((CAReloadRoleEvent) event).getRole().equals("Project Manager"))
			{
				
				carr.getActivityModelinView().reloadRoleList(CAModel.reloadRoleList(carr.getProcess()));
			}
			else
			{
				carr.getActivityModelinView().reloadRoleList1();
			}
		}
		
		else if(event instanceof CAUploadRoleEvent)
		{
			
			CAUploadRoleEvent caur = (CAUploadRoleEvent) event;
			CAModel.uploadRoleList(caur.getProcess(), caur.getRoleList());
		}
		
		else if(event instanceof CADeleteRoleeEvent)
		{
			CADeleteRoleeEvent cadr = (CADeleteRoleeEvent) event;
			CAModel.deleteRole(cadr.getProcess(), cadr.getRole());
		}
		
		else if(event instanceof CAReloadOptionEvent)
		{	
			CAReloadOptionEvent caro = (CAReloadOptionEvent) event;
			caro.getActivityModelinView().reloadOptionList(CAModel.reloadOptionList(caro.getProcess()));
			
		}
		else if(event instanceof CAUploadOptionEvent)
		{
			
			CAUploadOptionEvent cauo = (CAUploadOptionEvent) event;
			CAModel.uploadOptionList(cauo.getProcess(), cauo.getOptionList());
		}
		
		else if(event instanceof CADeleteOptionEvent)
		{
			CADeleteOptionEvent cado = (CADeleteOptionEvent) event;
			CAModel.deleteOption(cado.getProcess(), cado.getOption());
		}
		
		else if(event instanceof CAReloadArtifactEvent)
		{	
			CAReloadArtifactEvent cara = (CAReloadArtifactEvent) event;
			cara.getActivityModelinView().reloadArtifactList(CAModel.reloadArtifactList(cara.getProcess(), cara.getRole()));
			
		}
		
		else if(event instanceof CAReloadArtifactListEvent)
		{	
			CAReloadArtifactListEvent cara = (CAReloadArtifactListEvent) event;
			cara.getActivityModelinView().reloadArtifactInfo(CAModel.reloadArtifactList());
			
		}
		else if(event instanceof CAUploadArtifactEvent)
		{
			
			CAUploadArtifactEvent caua = (CAUploadArtifactEvent) event;
			CAModel.uploadArtifactList(caua.getProcess(),caua.getRole(), caua.getArtifactList());
		}
		
		else if(event instanceof CADeleteArtifactEvent)
		{
			
			CADeleteArtifactEvent cada = (CADeleteArtifactEvent) event;
			CAModel.deleteArtifact(cada.getProcess(), cada.getRole(), cada.getArtifact());
		}
		else if(event instanceof CAReloadStateListEvent)
		{	
			CAReloadStateListEvent cara = (CAReloadStateListEvent) event;
			cara.getActivityModelinView().reloadStateInfo(CAModel.reloadStateList(cara.getArtifact()));
			
		}
		else if(event instanceof CAReloadStateEvent)
		{	
			CAReloadStateEvent cara = (CAReloadStateEvent) event;
			cara.getActivityModelinView().reloadStateList(CAModel.reloadStateList(cara.getProcess(), cara.getRole(), cara.getArtifact()));	
		}
		else if(event instanceof CAReloadStateForActTabEvent)
		{	
			CAReloadStateForActTabEvent cara = (CAReloadStateForActTabEvent) event;
			cara.getActivityModelinView().reloadStateListForArtTab(CAModel.reloadStateList(cara.getProcess(), cara.getRole(), cara.getArtifact()));	
		}
		
		else if(event instanceof CAUploadStateEvent)
		{
			
			CAUploadStateEvent caus = (CAUploadStateEvent) event;
			CAModel.uploadStateList(caus.getProcess(),caus.getRole(), caus.getArtifact(), caus.getStateList());
		}
		else if(event instanceof PFReloadActivityEvent)
		{	
			PFReloadActivityEvent pfra = (PFReloadActivityEvent) event;
			pfra.getActivityModelinView().reloadActivityList(PFModel.reloadActivityList(pfra.getProcess(), pfra.getRole()));
			
		}
		
		else if(event instanceof PFUploadActivityEvent)
		{
			
			PFUploadActivityEvent pfua = (PFUploadActivityEvent) event;
			if(wfnr.verifyActivitiesName(pfua.getProcess(), pfua.getRole(), pfua.getActivityList()).isEmpty())
			{
				CAModel.uploadActivityList(pfua.getProcess(),pfua.getRole(), pfua.getActivityList());
				PFModel.uploadActivityList(pfua.getProcess(),pfua.getRole(), pfua.getActivityList());
			}
			else
			{
				pfua.getActivityModelinView().showMessageRule1(wfnr.verifyActivitiesName(pfua.getProcess(), pfua.getRole(), pfua.getActivityList()));
			}
		}
		
		else if(event instanceof PFDeleteActivityEvent)
		{
			
			PFDeleteActivityEvent pfda = (PFDeleteActivityEvent) event;
			PFModel.deleteActivity(pfda.getProcess(),pfda.getRole(), pfda.getActivity());
		}
		
		else if(event instanceof PFReloadTaskEvent)
		{	
			
			PFReloadTaskEvent pfrt = (PFReloadTaskEvent) event;
			pfrt.getActivityModelinView().reloadTaskList(PFModel.reloadTaskList(pfrt.getProcess(), pfrt.getRole(), pfrt.getActivity()));
			
		}
		
		else if(event instanceof PFUploadTaskEvent)
		{
			
			PFUploadTaskEvent pfut = (PFUploadTaskEvent) event;
			PFModel.uploadTaskList(pfut.getProcess(),pfut.getRole(),pfut.getActivity(), pfut.getTaskList());
		}
		
		else if(event instanceof PFDeleteTaskEvent)
		{
			
			PFDeleteTaskEvent pfdt = (PFDeleteTaskEvent) event;
			PFModel.deleteTask(pfdt.getProcess(),pfdt.getRole(),pfdt.getActivity(), pfdt.getTask());
		}
		
		else if(event instanceof PFReloadArtifactEvent)
		{	
			
			PFReloadArtifactEvent pfra = (PFReloadArtifactEvent) event;
			pfra.getActivityModelinView().reloadArtifactList1(PFModel.reloadArtifactList(pfra.getProcess(), pfra.getRole(), pfra.getActivity(), pfra.getTask()));
			
		}
		
		else if(event instanceof PFUploadArtifactEvent)
		{
			PFUploadArtifactEvent pfua= (PFUploadArtifactEvent) event;
			PFModel.uploadArtifactList(pfua.getProcess(),pfua.getRole(),pfua.getActivity(),pfua.getTask(), pfua.getArtifactList());
		}
		
		else if(event instanceof PFDeleteArtifactEvent)
		{
			PFDeleteArtifactEvent pfda= (PFDeleteArtifactEvent) event;
			PFModel.deleteArtifact(pfda.getProcess(),pfda.getRole(),pfda.getActivity(),pfda.getTask(), pfda.getArtifact(), pfda.getState());
		}
		
		/**
		 * 
		 */
		else if(event instanceof ActivityEvent)
		{	
			ActivityEvent te = (ActivityEvent) event;
			showActivityList(te.getRole(), te.getActor());// update actor view with showing list of activity definitions
			
		}
		/**
		 * Process State Machine Events
		 */
		else if(event instanceof ProcessCreateEvent)
		{
			ProcessCreateEvent pce = (ProcessCreateEvent) event;
			//ProcessDef pd = new ProcessDef();
			ProcessId++;
			pce.getWID().processId = ProcessId;
			pce.getWID().psm.controler=this;
			pce.getWID().psm.init(pce);
		}
		
		/**
		  *  Activity state Machine  Events
		  */
		else if(event instanceof ActivityCreateEvent)
		{
			ActivityCreateEvent actCreateEvent = (ActivityCreateEvent) event;
			ID++;
			actCreateEvent.setWID(PFModel.createActivityDefList(actCreateEvent.getActivity(), this));
			actCreateEvent.getWID().activityID = ID;
			actCreateEvent.getWID().projID = actCreateEvent.getProjID();
			//ID2ActivityMap.put(ID, actCreateEvent.getWID());
			claimedActWorkItemss.put(ID, actCreateEvent.getWID());
			actCreateEvent.getWID().asm.init(actCreateEvent);
			
			for (int i=0; i<actCreateEvent.getWID().taskList.size(); i++)
			{
				if (actCreateEvent.getWID().taskList.get(i).kind==TYPE.mandatory)
				{
					ID++;
					TaskCreateEvent taskCreateEvent = new TaskCreateEvent("");
					actCreateEvent.getWID().taskList.get(i).actor = actCreateEvent.getWID().actor;
					taskCreateEvent.setTask(actCreateEvent.getWID().taskList.get(i));
					taskCreateEvent.setID(ID);
					taskCreateEvent.setActor(actCreateEvent.getWID().actor);
					taskCreateEvent.setActivityID(actCreateEvent.getWID().activityID);
					actCreateEvent.getWID().taskList.get(i).activityID = actCreateEvent.getWID().activityID; 
					//ID2TaskMap.put(ID, actCreateEvent.getWID().taskList.get(i));
					claimedTaskWorkItemss.put(ID, taskCreateEvent.getTask());
					actCreateEvent.getWID().taskList.get(i).tsm.init(taskCreateEvent);
				}
			}
			/*
		
			ActivityCreateEvent actCreateEvent = (ActivityCreateEvent) event;
			ID++;
			actCreateEvent.setID(ID);
			ID2ActivityMap.put(ID, actCreateEvent.getWID());
			actCreateEvent.getWID().asm.init(actCreateEvent);
			
			for (int i=0; i<actCreateEvent.getWID().taskList.size(); i++)
			{
				TaskType manTask = actCreateEvent.getWID().taskList.get(i);
				if (actCreateEvent.getWID().taskList.get(i).kind==TYPE.mandatory)
				{
					
					ID++;
					TaskCreateEvent taskCreateEvent = new TaskCreateEvent("");
					actCreateEvent.getWID().taskList.get(i).actor = actCreateEvent.getWID().actor;
					taskCreateEvent.setTask(actCreateEvent.getWID().taskList.get(i));
					taskCreateEvent.setID(ID);
					taskCreateEvent.setActor(actCreateEvent.getWID().actor);
					taskCreateEvent.setActivityID(actCreateEvent.getID());
					actCreateEvent.getWID().taskList.get(i).activityID = actCreateEvent.getID(); 
					ID2TaskMap.put(ID, actCreateEvent.getWID().taskList.get(i));
					actCreateEvent.getWID().taskList.get(i).tsm.init(taskCreateEvent);
				}
			}
			
		*/}
		else if(event instanceof ActivityClaimEvent)
		{	
			ActivityClaimEvent actClaimEvent = (ActivityClaimEvent) event;
			ActivityDef wid = ActWorkItemss.get(actClaimEvent.getWI());
			ActWorkItemss.get(actClaimEvent.getWI()).actor = actClaimEvent.getActor();
			wid.asm.init(actClaimEvent);
			
		}
		
		else if(event instanceof ActivityCreateOptionEvent)
		{	
			
			ActivityCreateOptionEvent actCreateOptionEvent = (ActivityCreateOptionEvent) event;
			ActivityDef wid = claimedActWorkItemss.get(actCreateOptionEvent.getWI());
			actCreateOptionEvent.setWID(wid);
			// intialize the taskCreateEvent for TaskCreate transition
			TaskType optTask = null;
			for (int i=0; i<wid.taskList.size(); i++)
			{
				optTask = wid.taskList.get(i);
				if (optTask.kind==TYPE.option & optTask.option.equals(actCreateOptionEvent.getOption()))
				{
				
					ID++;
					TaskCreateEvent taskCreateEvent = new TaskCreateEvent("");
					optTask.actor = wid.actor;
					taskCreateEvent.setTask(optTask);
					taskCreateEvent.setID(ID);
					
					taskCreateEvent.setActivityID(actCreateOptionEvent.getWI());
					optTask.activityID = actCreateOptionEvent.getWI(); 
					claimedTaskWorkItemss.put(ID, optTask);
					optTask.tsm.init(taskCreateEvent);
				}
			}
			
			wid.asm.init(actCreateOptionEvent);
			
			/* Verify Option Impact
			 */
			//first find the affected tasks of the activity
			
			/*List<TaskType> impactedTaskList = new ArrayList<TaskType>();
			impactedTaskList = findImpactedTasks(wid, temp, actCreateOptionEvent.getOption());
			for(int k=0; k<impactedTaskList.size(); k++)
			{
				
				//TaskType impactedTask = impactedTaskList.get(k);
				OptionImpactEvent optionImpactEvent = new OptionImpactEvent("");
				optionImpactEvent.setTask(impactedTaskList.get(k));
				optionImpactEvent.setOption(actCreateOptionEvent.getOption());
				impactedTaskList.get(k).tsm.init(optionImpactEvent);
			}*/
		}
		
		else if(event instanceof ActivityCompleteEvent)
		{
			ActivityCompleteEvent actCompleteEvent = (ActivityCompleteEvent) event;
			claimedActWorkItemss.get(actCompleteEvent.getWI()).asm.init(actCompleteEvent);
		}
		 /**
		  *  Task state Machine Events
		  */
		 
		else if(event instanceof TaskCreateEvent)
			{	
			}
			
			// State: Created --> Reserved
		else if(event instanceof TaskClaimEvent)
			{
			}
			// State: Reserved --> InProgress
		else if(event instanceof TaskStartEvent)
			{	
				TaskStartEvent taskStartEvent = (TaskStartEvent) event;
				taskStartEvent.setTask(claimedTaskWorkItemss.get(taskStartEvent.getWI()));
				if(activityViewMap.get(taskStartEvent.getTask().actor).verifyInputValues()){
				taskStartEvent.getTask().tsm.init(taskStartEvent);}
		
			}
			
			// State: Reserved --> InProgress
		else if(event instanceof TaskCompleteEvent)
			{	
				TaskCompleteEvent taskCompleteEvent = (TaskCompleteEvent) event;
				taskCompleteEvent.setTask(claimedTaskWorkItemss.get(taskCompleteEvent.getWI()));
				if(activityViewMap.get(taskCompleteEvent.getTask().actor).verifyOutputValues()){
				taskCompleteEvent.getTask().tsm.init(taskCompleteEvent);}
			}	
		
		// Artifact Events
		else if(event instanceof ArtifactSelectEvent)
		{
			ArtifactSelectEvent artifactSelectEvent = (ArtifactSelectEvent) event;
			//TaskType task = ID2TaskMap.get(artifactSelectEvent.getWI());
			ArtifactType artifact = artifactSelectEvent.getArtifact();
			loadInputArtifact(claimedTaskWorkItemss.get(artifactSelectEvent.getWI()), artifact);
			switchTaskExeInfo(artifactSelectEvent.getWI());
			
		}
		
		else if(event instanceof ArtifactUploadEvent)
		{
			ArtifactUploadEvent artifactUploadEvent = (ArtifactUploadEvent) event;
			loadOutputArtifact(claimedTaskWorkItemss.get(artifactUploadEvent.getWI()), artifactUploadEvent.getArtifact());
			switchTaskExeInfo(artifactUploadEvent.getWI());
			artifactUploadEvent.getArtifact().artifactSM.getInstance().controler = this;
			artifactUploadEvent.getArtifact().artifactSM.getInstance().setState(artifactState.initial);
			artifactUploadEvent.getArtifact().artifactSM.getInstance().init(artifactUploadEvent);
			
			
		}
		
		else if(event instanceof DBUpdateEvent)
		{
			DBUpdateEvent dbUpdEvent = (DBUpdateEvent) event;
			ArtifactType artifact = dbUpdEvent.getArtifact();
			artifact.artifactSM.getInstance().controler = this;
			artifact.artifactSM.getInstance().setState(artifactState.initial);
			artifact.artifactSM.getInstance().init(dbUpdEvent);
		}
		
		/* 
		 * Change Events
		 */
		else if(event instanceof ChangeEvaluateRequest)
		{
			ChangeEvaluateRequest cev = (ChangeEvaluateRequest) event;
			activityViewMap.get(cev.getChangeRequest().resHres.actor).initializeChangeEvaluateForm(cev.getChangeRequest());
			
		}
		
		else if(event instanceof ChangeRequestEvent)
		{
			ChangeRequestEvent cre = (ChangeRequestEvent) event;
			cre.getActivityListView().initializeChangeRequestForm(cre.getChangeRequest());
		}
		
		else if(event instanceof SendChangeRequestEvent)
		{
			SendChangeRequestEvent scre = (SendChangeRequestEvent) event;
			cmv.reloadChangeActionReqInfo(scre.getChangeRequest());
			
		}
		
		else if(event instanceof ChangeRequestActionEvent)
		{
			ChangeRequestActionEvent crae = (ChangeRequestActionEvent) event;
			ChangeRequest crr = pdgModel.findChangeResponsible(crae.getChangeRequest());
			if(activityViewMap.get(crr.resHres.actor)!=null){
			activityViewMap.get(crr.resHres.actor).reloadChangeActionTable(crr);}
		}
		
		else if(event instanceof SendChangeRequestResEvent)
		{
			SendChangeRequestResEvent scrre = (SendChangeRequestResEvent) event;
			if(activityViewMap.get(scrre.getChangeRequest().resHres.actor)!=null){
				activityViewMap.get(scrre.getChangeRequest().resHres.actor).updateStatuse(scrre.getChangeRequest());}
			cmv.reloadChangeSignalReqInfo(scrre.getChangeRequest());
		}
		
		else if(event instanceof ChangeImpactAnalysisEvent)
		{
			ChangeImpactAnalysisEvent ciae = (ChangeImpactAnalysisEvent) event;
			//cmv.reloadChangeImpactAnalysisInfo(pdgModel.findAffectedElements(ciae.getChangeRequest(), this));
			cmv.reloadChangeImpact();
		}
		
		else if(event instanceof ChangeInformActorsEvent)
		{
			ChangeInformActorsEvent ciae = (ChangeInformActorsEvent) event;
			for (int i=0; i<ciae.getChangeRequest().affectedHres.size(); i++)
			{
				activityViewMap.get(ciae.getChangeRequest().affectedHres.get(i).actor).reloadChangeSignalTable(ciae.getChangeRequest());
			}
			sendEmail(ciae.getChangeRequest());
		}
		
		/*
		 * Process Monitoring Events
		 */
		else if(event instanceof PMReloadProcessEvent)
		{
			PMReloadProcessEvent pmrp = (PMReloadProcessEvent) event;
			pmrp.getProcessMonitoringView().renderProcessInfo(pdgModel.renderMonitoringUI(pmrp.getId()));
		}
		
	}
	
	

//-----------   // State Machine Actions. //  Event Handler----------------------------------------------------------------------
//------------------- Create Process Instance----------------------------------------------------------------
	
	public void createProcessUpdateView(ProcessDef wid)
	{
		// here we show the process instance in all process actors views
		//*****// Later must be resovled to only be shown in process actors view, those who participate in the process
		Iterator it = activityViewMap.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry pair = (Map.Entry)it.next();
			((ActivityListView) pair.getValue()).reloadProcessInsTable(wid);
		}
		 
			//activityViewMap.get(wid.initiator).reloadClaimedActivityInsTable(reloadActIns(claimedActWorkItemss, wid.initiator));
		
		
	}
	
	public void createProcessMonitoringUpdateView(ProcessDef wid)
	{
		
		pmv.reloadProcessList(wid);
		
	}

	// Handler to send the list of Task (definition) to the actor Taskdefinition view (UI)
	public void showActivityList(String role, String actor )
	{
		if(role.equals("Project Manager"))
		{
			DefaultListModel model = (DefaultListModel) activityViewMap.get(actor).activityWIList.getModel();
			model.clear();
			PFModel.createActivityDefList2(role, actor, model);
		}
		else
		{
			DefaultListModel model = (DefaultListModel) activityViewMap.get(actor).activityWIList.getModel();
			model.clear();
			PFModel.createActivityDefList1(role, actor, model);
		}
	}
	
	
//------------------- Create Task Instance----------------------------------------------------------------
	/**
	 * Activty instance is created in three steps: 1) internal activity definition  2) Execute Work Item
	 * @param wid
	 */
	public void createActivityUpdateView(ActivityDef wid)
	{
		activityViewMap.get(wid.actor).reloadClaimedActivityInsTable(reloadActIns(claimedActWorkItemss, wid.actor));
		
		// create the task panel
		TaskPanel tPanle = new TaskPanel(wid.activityID, activityViewMap.get(wid.actor));
		taskPanelViewMap.put(wid.activityID, tPanle);
	}
//-----------------------------------------------------------------------------------------------------	
	public void createActivityUpdateView1(String role)
	{
		for (Iterator<String> iterator = activityViewMap.keySet().iterator(); iterator.hasNext(); ) 
		{
			String actor = iterator.next();
			if (actor!=null)
			{
				if(activityViewMap.get(actor).hres.role.equals(role))
				{	
					activityViewMap.get(actor).reloadActivityInsTable(reloadActIns2(ActWorkItemss, role));
				}
			}
		}
		
		
		
		// create the task panel

	}
//------------------------------------------------------------------------------------------------------
	public void activityClaimUpdateView(long activityID, String actor)
	{
		// update Claimed Activity Instance View
		claimedActWorkItemss.put(activityID, ActWorkItemss.get(activityID));
		claimedActWorkItemss.get(activityID).actor = actor;
		ActWorkItemss.remove(activityID);
		activityViewMap.get(actor).reloadClaimedActivityInsTable(reloadActIns(claimedActWorkItemss, actor));
		createActivityUpdateView1(claimedActWorkItemss.get(activityID).role);
		// create the task panel
		TaskPanel tPanle = new TaskPanel(activityID, activityViewMap.get(actor));
		taskPanelViewMap.put(activityID, tPanle);
		//create task exe panel
		for(int i=0; i<claimedActWorkItemss.get(activityID).taskList.size(); i++)
		{
			claimedActWorkItemss.get(activityID).taskList.get(i).actor = actor;
			TaskInstanceExecutionPanel tExePanel = new TaskInstanceExecutionPanel(tPanle.actListView, tPanle, 
					claimedActWorkItemss.get(activityID).taskList.get(i).taskID);
			taskExePanelViewMap.put(claimedActWorkItemss.get(activityID).taskList.get(i).taskID, tExePanel);
		}
	 }
//----------------------------------------------------------------------------------------------------------
	public void createTaskUpdateView(TaskType task)
	{
		/**
		 * here engine Instantiates the task
		 */
		if(task.kind==TYPE.mandatory)
		{
			claimedTaskWorkItemss.put(task.taskID, task);
			TaskPanel tPanle = taskPanelViewMap.get(task.activityID);
			if(tPanle!=null)
			{
				TaskInstanceExecutionPanel tExePanel = new TaskInstanceExecutionPanel(tPanle.actListView, tPanle, task.taskID);
				taskExePanelViewMap.put(task.taskID, tExePanel);
			}
		}
		
		if(task.kind==TYPE.option)
		{
			claimedTaskWorkItemss.put(task.taskID, task);
			TaskPanel tPanle = taskPanelViewMap.get(task.activityID);
			if(tPanle!=null)
			{
				TaskInstanceExecutionPanel tExePanel = new TaskInstanceExecutionPanel(tPanle.actListView, tPanle, task.taskID);
				taskExePanelViewMap.put(task.taskID, tExePanel);
			}
		}
		
	}

//-------------------------------------------------------------------------------------------------------------------------------------
	public void activityCreateOptionView(ActivityDef wid)
	{
		
		TaskPanel tPanle = taskPanelViewMap.get(wid.activityID);
		
		System.out.println("hey is nuldd heuwdf");
		
		tPanle.reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, wid.activityID),reloadTaskInsList(waitingTaskWorkItemss, wid.activityID),
				claimedActWorkItemss.get(wid.activityID).option);
		//activityViewMap.get(claimedActWorkItemss.get(wid.activityID).actor).taskTable.setModel(tPanle.taskTableModel);
		
		
	}
//-------------------------------------------------------------------------------------------------------------------------------------
	public void activityCompleteView(long activityID)
	{
		String actor = claimedActWorkItemss.get(activityID).actor;
		claimedActWorkItemss.remove(activityID);
		activityViewMap.get(actor).reloadClaimedActivityInsTable(reloadActIns(claimedActWorkItemss, actor));
		
	}
//-------------------------------------------------------------------------------------------------------------------------------------	
	

//-------------------------------------------------------------------------------------------------------------------------------------	
	public void taskStartView(long taskID)
    {
		// update  View
		
		TaskInstanceExecutionPanel tExePanel = taskExePanelViewMap.get(taskID);
  		TaskType task = claimedTaskWorkItemss.get(taskID);
  		tExePanel.reloadTaskExeInfoOut(reloadArtifactListOut(task, taskID));
  		activityViewMap.get(claimedActWorkItemss.get(task.activityID).actor).startButt.setEnabled(false);
  		//activityViewMap.get(claimedActWorkItemss.get(task.activityID).actor).inputTable.setRowSelectionAllowed(false);
  		activityViewMap.get(claimedActWorkItemss.get(task.activityID).actor).taskCompleteButt.setEnabled(true);
  		activityViewMap.get(claimedActWorkItemss.get(task.activityID).actor).showWarningMessage1();
    	
    }
//------------------------------------------------------------------------------------------------------------------------------------------------
	public void suspendTask(TaskType task, List<ArtifactType> notOkArtifact)
	{
		// registering in the inetrnal Ram (Not PDG)
		/*ID2WaitingTaskMap.put(workItem.getId(), task);
		ID2ArtifactMap.put(workItem.getId(), notOkArtifact);
		for(int i=0; i<notOkArtifact.size(); i++)
		{
			Artifact2IDMap.put(notOkArtifact.get(i), workItem.getId());
		}*/
		
		TaskPanel tPanel = taskPanelViewMap.get(task.activityID);
		claimedTaskWorkItemss.remove(task.taskID);
		waitingTaskWorkItemss.put(task.taskID, task);
		tPanel.reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, task.activityID), reloadTaskInsList(waitingTaskWorkItemss, task.activityID),
				claimedActWorkItemss.get(task.activityID).option);
		
		
	}
//------------------------------------------------------------------------------------------------------------------------------------------------	
	public void taskComplete(long taskID)
    {
		long activityID = claimedTaskWorkItemss.get(taskID).activityID;
		String actor = claimedTaskWorkItemss.get(taskID).actor;
		TaskPanel tpanel = taskPanelViewMap.get(claimedTaskWorkItemss.get(taskID).activityID);
		claimedTaskWorkItemss.remove(taskID);
		tpanel.reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, activityID), reloadTaskInsList(waitingTaskWorkItemss, activityID),
				claimedActWorkItemss.get(activityID).option);
		activityViewMap.get(actor).showWarningMessage2();
		
		TaskInstanceExecutionPanel tx = taskInstanceExecutionViewMap.get(taskID);
		tx = null;
		taskInstanceExecutionViewMap.remove(taskID);
		
    }
	
//---------------------------------------------------------------------------------------------------------------------------------------------	
	public void artifactUpload(ArtifactType artifact, List<Long> wakeupTasks, long taskID)
	{
		taskWakeUp(wakeupTasks);
		//activityProvoke(claimedTaskWorkItemss.get(taskID), artifact);
	}
//---------------------------------------------------------------------------------------------------------------------------------------------	
	public void artifactDBUpload(ArtifactType artifact, List<Long> wakeupTasks, long projID)
	{
		taskWakeUp(wakeupTasks);
		//activityDBProvoke(artifact, projID);
	}	
//----------------------------------------------------------------------------------------------------------------------------------------------	
	public void taskWakeUp(List<Long> wakeupTasks)
    {
		if(!wakeupTasks.isEmpty())
		{
			for(int i=0 ; i<wakeupTasks.size() ; i++)
			{
				System.out.println("task id isss:" + wakeupTasks.get(i));
				if(waitingTaskWorkItemss.get(wakeupTasks.get(i))!=null)
				{
					TaskWakeUpEvent taskWakeUp = new TaskWakeUpEvent();
					TaskType task = waitingTaskWorkItemss.get(wakeupTasks.get(i));
					taskWakeUp.setTask(task);
					task.tsm.init(taskWakeUp);
					long activityID = waitingTaskWorkItemss.get(wakeupTasks.get(i)).activityID;
					TaskPanel tpanel = taskPanelViewMap.get(activityID);
					claimedTaskWorkItemss.put(findWorkItem(waitingTaskWorkItemss, wakeupTasks.get(i)), task);
					waitingTaskWorkItemss.remove(findWorkItem(waitingTaskWorkItemss, wakeupTasks.get(i)));
					tpanel.reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, activityID), 
							reloadTaskInsList(waitingTaskWorkItemss, activityID), claimedActWorkItemss.get(activityID).option);
					activityViewMap.get(taskWakeUp.getTask().actor).startButt.setEnabled(true);
				}
			}
		}
    }
//-----------------------------------------------------------------------------------------------------------------------------------------------
	public void activityProvoke(TaskType task, ArtifactType artifact)
	{
		
		List<ProvokeType> provokList = PFModel.findProvokedActivities(artifact, claimedActWorkItemss.get(task.activityID).process, this);

		for (int k=0; k<provokList.size(); k++)
		{
			
			if(pdgModel.verifyProvokeEvent(claimedActWorkItemss.get(task.activityID).projID, provokList.get(k).wid.name))
			{
				ActivityProvokeEvent activityProvEve = new ActivityProvokeEvent();
				ID++;
				activityProvEve.setWID(provokList.get(k).wid);
				activityProvEve.setProjID(claimedActWorkItemss.get(task.activityID).projID);
				activityProvEve.setID(ID);
				ActWorkItemss.put(ID, activityProvEve.getWID());
				activityProvEve.getWID().asm.init(activityProvEve);
				
				for (int i=0; i<activityProvEve.getWID().taskList.size(); i++)
				{
					//TaskType manTask = activityProvEve.getWID().taskList.get(i);
					if (activityProvEve.getWID().taskList.get(i).kind==TYPE.mandatory)
					{
						
						ID++;
						TaskCreateEvent taskCreateEvent = new TaskCreateEvent("");
						activityProvEve.getWID().taskList.get(i).actor = activityProvEve.getWID().actor;
						taskCreateEvent.setTask(activityProvEve.getWID().taskList.get(i));
						taskCreateEvent.setID(ID);
						taskCreateEvent.setActor(activityProvEve.getWID().actor);
						taskCreateEvent.setActivityID(activityProvEve.getID());
						activityProvEve.getWID().taskList.get(i).activityID = activityProvEve.getID(); 
						claimedTaskWorkItemss.put(ID, taskCreateEvent.getTask());
						if(taskCreateEvent.getTask().type.equals(provokList.get(k).task))
						{
							for(int j=0; j<taskCreateEvent.getTask().inArtifactList.size(); j++)
							{
								if(taskCreateEvent.getTask().inArtifactList.get(j).type.equals(artifact.type))
								{
									taskCreateEvent.getTask().inArtifactList.get(j).instanceName.add(artifact.instanceName.get(0));
								}
							}
						}
						
						
						activityProvEve.getWID().taskList.get(i).tsm.init(taskCreateEvent);
					}
				}
			}	
			
						
		}		
			
		
	}
//------------------------------------------------------------------------------------------------------------------
	public void activityDBProvoke(ArtifactType artifact, long projID)
	{

		List<ProvokeType> provokList = PFModel.findProvokedActivities(artifact, artifact.usage, this);

		for (int k=0; k<provokList.size(); k++)
		{
			
			if(pdgModel.verifyProvokeEvent(projID, provokList.get(k).wid.name))
			{
				ActivityProvokeEvent activityProvEve = new ActivityProvokeEvent();
				ID++;
				activityProvEve.setWID(provokList.get(k).wid);
				activityProvEve.setProjID(projID);
				activityProvEve.setID(ID);
				ActWorkItemss.put(ID, activityProvEve.getWID());
				activityProvEve.getWID().asm.init(activityProvEve);
				
				for (int i=0; i<activityProvEve.getWID().taskList.size(); i++)
				{
					//TaskType manTask = activityProvEve.getWID().taskList.get(i);
					if (activityProvEve.getWID().taskList.get(i).kind==TYPE.mandatory)
					{
						
						ID++;
						TaskCreateEvent taskCreateEvent = new TaskCreateEvent("");
						activityProvEve.getWID().taskList.get(i).actor = activityProvEve.getWID().actor;
						taskCreateEvent.setTask(activityProvEve.getWID().taskList.get(i));
						taskCreateEvent.setID(ID);
						taskCreateEvent.setActor(activityProvEve.getWID().actor);
						taskCreateEvent.setActivityID(activityProvEve.getID());
						activityProvEve.getWID().taskList.get(i).activityID = activityProvEve.getID(); 
						claimedTaskWorkItemss.put(ID, taskCreateEvent.getTask());
						if(taskCreateEvent.getTask().type.equals(provokList.get(k).task))
						{
							for(int j=0; j<taskCreateEvent.getTask().inArtifactList.size(); j++)
							{
								if(taskCreateEvent.getTask().inArtifactList.get(j).type.equals(artifact.type))
								{
									taskCreateEvent.getTask().inArtifactList.get(j).instanceName.add(artifact.instanceName.get(0));
								}
							}
						}
						
						
						activityProvEve.getWID().taskList.get(i).tsm.init(taskCreateEvent);
					}
				}
			}	
			
						
		}		
		
	}

//-----------------------------------------------------------------------------------------------------------------------------------------
	public String findProvoker(ArtifactType artifact)
	{
		String pkr = null;
		generateXML info = new generateXML("resources/info.xml");
		NodeList  xmlProvokerList = info.doc.getElementsByTagName("provoker");
		for (int i = 0; i < xmlProvokerList.getLength(); i++) 
		{
			NodeList artifactchilds = xmlProvokerList.item(i).getChildNodes();
			
			for (int j = 0; j < artifactchilds.getLength(); j++) 	
			{
				if(artifactchilds.item(j).getNodeName().equals("artifact"))
				{
					if(artifactchilds.item(j).getAttributes().getNamedItem("name").getNodeValue().equals(artifact.type) & 
							artifactchilds.item(j).getAttributes().getNamedItem("state").getNodeValue().equals(artifact.state))
					{
						pkr = xmlProvokerList.item(i).getAttributes().getNamedItem("name").getNodeValue();
					}
				}
			}
			
		}
		
		return pkr;
	}
//-----------------------------------------------------------------------------------------------------------------------------------------------	
	public void createOrganizational()
	{
		/**
		 * here engine create actor instances exist in the organizational model
		 */
	}


  	public List<ActivityInsWrapper> reloadActIns(Map<Long, ActivityDef> workItemss, String actor)
	{
		
		List<ActivityInsWrapper> result = new ArrayList<ActivityInsWrapper>();
		for (Iterator<Long> iterator = workItemss.keySet().iterator(); iterator.hasNext(); ) 
		{
			long workItem = iterator.next();
			
			if(workItemss.get(workItem).actor.equals(actor))
			{	
				result.add(new ActivityInsWrapper(workItem, workItemss.get(workItem)));
			}
		}
		
		return result;
	}
  	
  	public List<ActivityInsWrapper> reloadActIns2(Map<Long, ActivityDef> workItemss, String role)
	{
		
		List<ActivityInsWrapper> result = new ArrayList<ActivityInsWrapper>();
		for (Iterator<Long> iterator = workItemss.keySet().iterator(); iterator.hasNext(); ) 
		{
			long workItem = iterator.next();
			
			if(workItemss.get(workItem).role.equals(role))
			{	
				result.add(new ActivityInsWrapper(workItem, workItemss.get(workItem)));
			}
		}
		
		return result;
	}
//------------------------------------------------------------------------------------------------------
  	public List<TaskInsWrapper> reloadTaskInsList(Map<Long, TaskType> workItemss, Long actID)
	{
  		
  		List<TaskInsWrapper> result = new ArrayList<TaskInsWrapper>();
  		
		for (Iterator<Long> iterator = workItemss.keySet().iterator(); iterator.hasNext(); ) 
		{
			long workItem = iterator.next();
			if (workItem!=-1)
			{
				if(workItemss.get(workItem).activityID == actID)
				{	
					result.add(new TaskInsWrapper(workItem, workItemss.get(workItem)));
				}
			}
		}
		
		return result;
  		
  		
	}
  	
  	public List<TaskType> reloadTaskInsList1(Map<Long, TaskType> workItemss, Long actID)
	{
  		
  		List<TaskType> result = new ArrayList<TaskType>();
  		TaskType tp;
		for (Iterator<Long> iterator = workItemss.keySet().iterator(); iterator.hasNext(); ) 
		{
			long workItem = iterator.next();
			if (workItem!=-1)
			{
				if(workItemss.get(workItem).activityID == actID)
				{	
//					result.add(new TaskInsWrapper(workItem, workItemss.get(workItem)));
				}
			}
		}
		
		return result;
  		
  		
	}
//------------------------------------------------------------------------------------------------------------  	
  	public long findWorkItem(Map<Long, TaskType> workItemss, Long ID)
	{

  		long work = 0;
		for (Iterator<Long> iterator = workItemss.keySet().iterator(); iterator.hasNext(); ) 
		{
			Long workItem = iterator.next();
			if (workItem!=null)
			{
				if(workItem==ID)
				{	
					work = workItem;
				}
			}
		}
		return work;
		
		
	}

//----------------------------------------------------------------------------------------------------------------
  	
  	/*public void switchTaskInfo(WorkItem workItem)
  	{
  		
  		//TaskPanel taskPanel = taskPanelViewMap.get(workItem.getParameter("ActivityID"));
  		taskPanelViewMap.get(workItem.getParameter("ActivityID")).reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, workItem.getId()), reloadTaskInsList(waitingTaskWorkItemss, workItem.getId()),
  				claimedActWorkItemss.get(workItem.getId()).option);
  		activityViewMap.get(workItem.getParameter("Actor").toString()).optComboBox.setModel(taskPanelViewMap.get(workItem.getParameter("ActivityID")).optComboBox.getModel());
  		activityViewMap.get(workItem.getParameter("Actor").toString()).invalidate();
		activityViewMap.get(workItem.getParameter("Actor").toString()).validate();
  	}*/
  	
  	public void switchTaskInfo1(long activityID)
  	{
  		/*taskPanelViewMap.get(activityID).reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, activityID), reloadTaskInsList(waitingTaskWorkItemss, activityID),
  				claimedActWorkItemss.get(activityID).option);
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).taskTable.setModel(taskPanelViewMap.get(activityID).taskTableModel);
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).lblActivityInstanceView.setText("Activity Instance: " +claimedActWorkItemss.get(activityID).name);
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).optComboBox.setModel(taskPanelViewMap.get(activityID).optComboBox.getModel());
		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).invalidate();
		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).validate();*/
  		taskPanelViewMap.get(activityID).reloadTaskIns(reloadTaskInsList(claimedTaskWorkItemss, activityID), reloadTaskInsList(waitingTaskWorkItemss, activityID),
  				claimedActWorkItemss.get(activityID).option);
  		//System.out.println("s:" + claimedActWorkItemss.get(activityID).role + " " + claimedActWorkItemss.get(activityID).name );
  		taskPanelViewMap.get(activityID).reloadTaskDefs(PFModel.reloadTaskList1("Modify Testbench Wiring", claimedActWorkItemss.get(activityID).role, claimedActWorkItemss.get(activityID).name));
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).taskTable.setModel(taskPanelViewMap.get(activityID).taskTableModel);
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).taskDefTable.setModel(taskPanelViewMap.get(activityID).taskDefTableModel);
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).lblActivityInstanceView.setText("Activity Instance: " +claimedActWorkItemss.get(activityID).name);
  		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).optComboBox.setModel(taskPanelViewMap.get(activityID).optComboBox.getModel());
  		
		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).invalidate();
		activityViewMap.get(claimedActWorkItemss.get(activityID).actor).validate();
  	}
 //----------------------------------------------------------------------------------------------------------------  	
  	public void switchTaskExeInfo(long taskID)
	{
  		TaskType task = claimedTaskWorkItemss.get(taskID);
  		taskExePanelViewMap.get(taskID).reloadTaskExeInfo(reloadArtifactList(task, taskID));
  		if(task.tsm.getState()==taskState.created)
  			
  		{
  			taskExePanelViewMap.get(taskID).reloadTaskExeInfoOut1();
  			activityViewMap.get(task.actor).startButt.setEnabled(true);
  			activityViewMap.get(task.actor).outputComboBox.setSelectedIndex(-1);
  		}
  		
  		if(task.tsm.getState()==taskState.inProgress)
  		{
  			taskExePanelViewMap.get(taskID).reloadTaskExeInfoOut(reloadArtifactListOut(task, taskID));
  			activityViewMap.get(task.actor).taskCompleteButt.setEnabled(true);
  			
  			activityViewMap.get(task.actor).startButt.setEnabled(false);
  			activityViewMap.get(task.actor).outputComboBox.setSelectedIndex(-1);
  		}
  		activityViewMap.get(task.actor).taskInsLabel.setText("Task Instance: " + claimedTaskWorkItemss.get(taskID).type);
		
	}
//----------------------------------------------------------------------------------------------------------
  	public List<ArtifactWrapper> reloadArtifactList(TaskType task, long taskID)
	{
  		List<ArtifactWrapper> result = new ArrayList<ArtifactWrapper>();
		for (int i=0; i<task.inArtifactList.size(); i++ ) 
		{
			if(task.inArtifactList.get(i).option.equals("null"))
			{
				result.add(new ArtifactWrapper(task, task.inArtifactList.get(i)));
			}
			else if(claimedActWorkItemss.get(task.activityID).activeOptionsList.contains(task.inArtifactList.get(i).option))
			{
				result.add(new ArtifactWrapper(task, task.inArtifactList.get(i)));
			}
			
		}
  		/*List<String> activeOptionsList = new ArrayList<String>(Arrays.asList(pdgModel.getActiveOptions(ID2ActivityMap.get(task.activityID))));
  		  for (int i=0; i<task.inArtifactList.size(); i++ ) 
		{
			if(task.inArtifactList.get(i).option.equals("null"))
			{
				result.add(new ArtifactWrapper(workItem, task, task.inArtifactList.get(i)));
			}
			else if(activeOptionsList.contains(task.inArtifactList.get(i).option))
			{
				result.add(new ArtifactWrapper(workItem, task, task.inArtifactList.get(i)));
			}
			
		}
  		*/
		return result;
	}
//------------------------------------------------------------------------------------------------------------
 
  	public void reloadInputArtifactValues(String artifactType, long taskID)
  	   {
  			//List<String> artifactList = pdgModel.reloadInputArtifactValues(artifactType);
  		ArrayList<String> petStrings = new ArrayList();
  		switch(artifactType) {
		case "FICD": // source state
			petStrings.add("FICD1");
			petStrings.add("FICD2");
			break;
		case "Wiring Change Demand": // source state
			petStrings.add("WCD1");
			break;
  		
  	   case "Bench Specification": // source state
			petStrings.add("BS1");
			break;	
		case "Electrical Specification": // source state
			petStrings.add("ES1");
			break;
		case "Electrical Design Model": // source state
			petStrings.add("EDM1");
			break;
		case "Component Requirements": // source state
			petStrings.add("CR1");
			petStrings.add("CR2");
			break;
		case "Component": // source state
			petStrings.add("C1");
			petStrings.add("C2");
			break;
		
		}  	
  		
  		
  		activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).inputComboBox.setModel(new DefaultComboBoxModel(petStrings.toArray()));
  		
  		//activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).inputComboBox.setModel(new DefaultComboBoxModel(pdgModel.reloadInputArtifactValues(artifactType).toArray()));
  			activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).invalidate();
  			activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).validate();
  	   }	
//-------------------------------------------------------------------------------------------------------------------------------------
  	 
  	public void reloadOutputArtifactValues(String artifactType, long taskID)
  	   {
  			//List<String> artifactList = pdgModel.reloadOutputArtifactValues(artifactType);
  		ArrayList<String> petStrings = new ArrayList();
  		switch(artifactType) {
		case "FICD": // source state
			petStrings.add("FICD1");
			petStrings.add("FICD2");
			break;
		case "Wiring Change Demand": // source state
			petStrings.add("WCD1");
			break;
  		
  	   case "Bench Specification": // source state
			petStrings.add("BS1");
			break;	
		case "Electrical Specification": // source state
			petStrings.add("ES1");
			break;
		case "Electrical Design Model": // source state
			petStrings.add("EDM1");
			break;
		case "Component Requirements": // source state
			petStrings.add("CR1");
			petStrings.add("CR2");
			break;
		case "Component": // source state
			petStrings.add("C1");
			petStrings.add("C2");
			break;
		
		}  	
  		
  		activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).outputComboBox.setModel(new DefaultComboBoxModel(petStrings.toArray()));
  			/*activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).outputComboBox.setModel(new javax.swing.DefaultComboBoxModel
  																					(pdgModel.reloadOutputArtifactValues(artifactType).toArray()));*/
  			activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).invalidate();
  			activityViewMap.get(claimedTaskWorkItemss.get(taskID).actor).validate();
  	   }	
//-------------------------------------------------------------------------------------------------------------------------------------  	  	
  	public List<ArtifactWrapper> reloadArtifactListOut(TaskType task, long taskID)
	{
  		List<ArtifactWrapper> result = new ArrayList<ArtifactWrapper>();
		for (int i=0; i<task.outArtifactList.size(); i++ ) 
		{
			if(task.outArtifactList.get(i).option.equals("null"))
			{
				result.add(new ArtifactWrapper(task, task.outArtifactList.get(i)));
			}
			else if(claimedActWorkItemss.get(task.activityID).activeOptionsList.contains(task.outArtifactList.get(i).option))
			{
				result.add(new ArtifactWrapper(task, task.outArtifactList.get(i)));
			}
			
		}
  		/*List<String> activeOptionsList = new ArrayList<String>(Arrays.asList(pdgModel.getActiveOptions(ID2ActivityMap.get(task.activityID))));
  		  for (int i=0; i<task.inArtifactList.size(); i++ ) 
		{
			if(task.inArtifactList.get(i).option.equals("null"))
			{
				result.add(new ArtifactWrapper(workItem, task, task.inArtifactList.get(i)));
			}
			else if(activeOptionsList.contains(task.inArtifactList.get(i).option))
			{
				result.add(new ArtifactWrapper(workItem, task, task.inArtifactList.get(i)));
			}
			
		}
  		*/
		return result;
	}
  	
  	public void loadInputArtifact(TaskType task, ArtifactType artifact)
  	{
  		for(int i=0; i<task.inArtifactList.size(); i++)
  		{
  			if(task.inArtifactList.get(i).type.equals(artifact.type))
  			{
  				if(task.inArtifactList.get(i).state.equals(artifact.state))
  				// here just for single Input Artifact
  				{
  					task.inArtifactList.get(i).instanceName.add(artifact.instanceName.get(0));
  				}
  			}
  		}
  		
  	}
  	
  	public void loadOutputArtifact(TaskType task, ArtifactType artifact)
  	{
  		for(int i=0; i<task.outArtifactList.size(); i++)
  		{
  			if(task.outArtifactList.get(i).type.equals(artifact.type))
  			{
  				// here just for single Input Artifact
  				if(task.outArtifactList.get(i).state.equals(artifact.state))
  	  				// here just for single Input Artifact
  	  			{
  					task.outArtifactList.get(i).instanceName.add(artifact.instanceName.get(0));
  	  			}
  			}
  		}
  	}
  	
  	public List<TaskType> findImpactedTasks(ActivityDef wid, TaskType optTask, String option)
  	{
  		List<TaskType> taskList = new ArrayList<TaskType>();
		for (int i=0; i<wid.taskList.size(); i++)
		{
			TaskType task = wid.taskList.get(i);
			if(!task.type.equals(optTask.type))
			{
				// here we apply the ADD INPUT  Impact
				for (int j=0; j<task.inArtifactList.size(); j++)
				{
					if(task.inArtifactList.get(j).option.equals(option))
					{
						if(!taskList.contains(task)){taskList.add(task);}
					}
				}
			}
			
		}
		return taskList;
  	}
//------------------------------------------------------------------------------------------------
  	public List<MonitType> renderMonitoringUI(String processId)
  	{
  		List<MonitType> mtList;
  		mtList = pdgModel.renderMonitoringUI(processId);
  		
  		return mtList;
  		
  	}
  	
//-------------------------------------------------------------------------------------------------  	
  	public void sendEmail(ChangeRequest cr)
  	{
  		for(int j=0; j<cr.affectedHres.size(); j++)
  		{
  			String from = "capeprocessenvironment@gmail.com";
	         String pass = "fardadfardad3860";
	         String[] to = { cr.affectedHres.get(j).email }; // list of recipient email addresses
	         String subject = "CAPE Change Alert: Change Signal ID: " + cr.ID.toString();  
	         String body = "You are concern of a Change with follwoing info:"+ "\n" 
	         + "Change Responsible: " + cr.resHres.actor + "\n" 
	         + "Change Detector: " + cr.initHres.actor + "\n" 
	         + "Changed Artifact: " + cr.artifactList.get(0).instanceName.get(0)+ "\n" 
	    	 + "Comment of Change Responsible: " + cr.resComment + "\n" 
	    	 + "Comment of Change Detector: " + cr.initComment;
	         
	   
	         Properties props = System.getProperties();
	         String host = "smtp.gmail.com";
	         props.put("mail.smtp.starttls.enable", "true");
	         props.put("mail.smtp.host", host);
	         props.put("mail.smtp.user", from);
	         props.put("mail.smtp.password", pass);
	         props.put("mail.smtp.port", "587");
	         props.put("mail.smtp.auth", "true");
	
	         Session session = Session.getDefaultInstance(props);
	         MimeMessage message = new MimeMessage(session);
	
	         try {
	             message.setFrom(new InternetAddress(from));
	             InternetAddress[] toAddress = new InternetAddress[to.length];
	
	             // To get the array of addresses
	             for( int i = 0; i < to.length; i++ ) {
	                 toAddress[i] = new InternetAddress(to[i]);
	             }
	
	             for( int i = 0; i < toAddress.length; i++) {
	                 message.addRecipient(Message.RecipientType.TO, toAddress[i]);
	             }
	
	             message.setSubject(subject);
	             message.setText(body);
	             Transport transport = session.getTransport("smtp");
	             transport.connect(host, from, pass);
	             transport.sendMessage(message, message.getAllRecipients());
	             transport.close();
	         }
	         catch (AddressException ae) {
	             ae.printStackTrace();
	         }
	         catch (MessagingException me) {
	             me.printStackTrace();
	         }
  	     }
  		}
  		 
  		
  	
  	
}

	
	