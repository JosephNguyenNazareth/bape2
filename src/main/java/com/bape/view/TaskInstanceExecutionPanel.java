package com.bape.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;


import com.bape.event.ActivityCreateOptionEvent;
import com.bape.event.TaskClaimEvent;
import com.bape.event.TaskStartEvent;
import com.bape.type.ActivityInsWrapper;
import com.bape.type.ArtifactWrapper;
import com.bape.type.TaskInsWrapper;
import com.bape.type.TaskType;

public class TaskInstanceExecutionPanel 
{
	ActivityListView actListView;
	TaskPanel taskPanel;
	
	public DefaultListModel inputArtModel = new DefaultListModel();
	public JList inputArtList = new JList(inputArtModel);
	public DefaultListModel outputArtModel = new DefaultListModel();
	public JList outputArtList = new JList(outputArtModel);
	public TaskStartEvent startButt = new TaskStartEvent("Start");
	public JTextField inputTextField;
	public JTextField outputTextField;
	public Long taskID;
	public JPanel taskExePanel = new JPanel();
	public JLabel taskInsLabel;
	
	String[] artifactColumnNames = {"Type", "State", "Usage", "Value"};
    String[][] artifactData = {,}; 
    public DefaultTableModel artifactTableModel = new DefaultTableModel(artifactData, artifactColumnNames);
 	public JTable inputTable = new JTable(artifactTableModel);
 	
 	public DefaultTableModel artifactOutTableModel = new DefaultTableModel(artifactData, artifactColumnNames);
 	public JTable outputTable = new JTable(artifactOutTableModel);
 	
 	
	public TaskInstanceExecutionPanel(ActivityListView actListView, TaskPanel taskPanel, long taskID )
	{
		this.actListView = actListView;
		this.taskPanel = taskPanel;
		this.taskID = taskID;
		
	}
	
	public void reloadTaskExeInfo(List<ArtifactWrapper> inArtifactResult)
	{
		artifactTableModel.setRowCount(0);
  		for(int i=0;i<inArtifactResult.size();i++)
		{
  			if(inArtifactResult.get(i).getArtifact().instanceName.isEmpty())
  			{
  			artifactTableModel.addRow(new String[]{inArtifactResult.get(i).getArtifact().type, 
  					inArtifactResult.get(i).getArtifact().state, inArtifactResult.get(i).getArtifact().usage, 
  					" " });
  			}
  			else
  			{
  				artifactTableModel.addRow(new String[]{inArtifactResult.get(i).getArtifact().type, 
  	  					inArtifactResult.get(i).getArtifact().state, inArtifactResult.get(i).getArtifact().usage, 
  	  					inArtifactResult.get(i).getArtifact().instanceName.get(0) });
  				
  			}
  			
		}
  		inputTable.setModel(artifactTableModel);
  		actListView.inputTable.setModel(artifactTableModel);
	}
	
	public void reloadTaskExeInfoOut(List<ArtifactWrapper> outArtifactResult)
	{
		artifactOutTableModel.setRowCount(0);
  		for(int i=0;i<outArtifactResult.size();i++)
		{
  			if(outArtifactResult.get(i).getArtifact().instanceName.isEmpty())
  			{
  			artifactOutTableModel.addRow(new String[]{outArtifactResult.get(i).getArtifact().type, 
  					outArtifactResult.get(i).getArtifact().state, outArtifactResult.get(i).getArtifact().usage, 
  					" "});
  			}
  			else
  			{
  				artifactOutTableModel.addRow(new String[]{outArtifactResult.get(i).getArtifact().type, 
  	  					outArtifactResult.get(i).getArtifact().state, outArtifactResult.get(i).getArtifact().usage, 
  	  					outArtifactResult.get(i).getArtifact().instanceName.get(0)});
  			}
		}
  		outputTable.setModel(artifactOutTableModel);
  		actListView.outputTable.setModel(artifactOutTableModel);
	}
	public void reloadTaskExeInfoOut1()
	{
		artifactOutTableModel.setRowCount(0);
  		
  		outputTable.setModel(artifactOutTableModel);
  		actListView.outputTable.setModel(artifactOutTableModel);
  		
	}
}