package com.bape.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import com.bape.controler.ProcessEngineController;
import com.bape.event.PMReloadProcessEvent;
import com.bape.type.MonitType;
import com.bape.type.ProcessDef;
import com.bape.type.humanResourceType;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ProcessMonitoringView 
{
	JTabbedPane tabbedPane;
	JSplitPane outer, inner;
	GridBagLayout gbl = new GridBagLayout();;
	GridBagConstraints gbc = new GridBagConstraints();
	JSplitPane splitPane;
	String[] pcolumnNames = {"ProcessID", "Name", "State", "Initiator", "Start", "End", "Duration"};
    String[][] pdata = {,}; 
	
	String[] columnNames = {"Id", "Name", "ActivityID", "ActivityName","Actor", "State",  "Start", "End", "Duration"};
    String[][] data = {,}; 
    String[] acolumnNames = {"Id", "Name", "Actor", "State", "Completion Percentage"};
    String[][] adata = {,}; 
    public DefaultTableModel processTableModel = new DefaultTableModel(pdata, pcolumnNames);
   	public JTable processTable = new JTable(processTableModel);
   	
    public DefaultTableModel activeTaskTableModel = new DefaultTableModel(data, columnNames);
   	public JTable activeTaskTable = new JTable(activeTaskTableModel);
   	
   	
   	public DefaultTableModel completedTaskTabelModel = new DefaultTableModel(data, columnNames);
   	public JTable completedTaskTabel = new JTable(completedTaskTabelModel);
   	
   	public DefaultTableModel activeArtifactTabelModel = new DefaultTableModel(adata, acolumnNames);
   	public JTable activeArtifactTabel = new JTable(activeArtifactTabelModel);
   	
   	public DefaultTableModel completedArtifactTabelModel = new DefaultTableModel(adata, acolumnNames);
   	public JTable completedArtifactTabel = new JTable(activeArtifactTabelModel);
   	ProcessEngineController controler;
   	ProcessMonitoringView pmv;
   	public String st="Feri";
	public ProcessMonitoringView(ProcessEngineController controler, humanResourceType hres, JTabbedPane tabbedPane)
	{
		this.tabbedPane = tabbedPane;
		this.controler = controler;
		pmv = this;
		initializeComponent();
	}
	
	//------------------------------------------------------------
	
	public void initializeComponent()
	{
		EventQueue.invokeLater(new Runnable()
	    {
			@Override
			public void run()
	        {
				// define the UI
	        	JPanel mainPanel = new JPanel();
	        	GridBagLayout gbl_mainPanel = new GridBagLayout();
	    		gbl_mainPanel.columnWidths = new int[]{0, 0};
	    		gbl_mainPanel.rowHeights = new int[]{0, 0};
	    		gbl_mainPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	    		gbl_mainPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
	    		mainPanel.setLayout(gbl_mainPanel);
	        	/*mainPanel.setLayout(new FormLayout(new ColumnSpec[] {
	    				FormSpecs.RELATED_GAP_COLSPEC,
	    				ColumnSpec.decode("default:grow"),},
	    			new RowSpec[] {
	    				FormSpecs.RELATED_GAP_ROWSPEC,
	    				RowSpec.decode("default:grow"),}));
	 			*/
	    		ImageIcon monitoringIcon = new ImageIcon("resources/monitoring.jpeg");
	    		tabbedPane.addTab("Monitoring", monitoringIcon, mainPanel,null);
	    		outer = new JSplitPane();
	    		outer.setOrientation(JSplitPane.VERTICAL_SPLIT);
	    		GridBagConstraints gbc_outer = new GridBagConstraints();
	    		gbc_outer.fill = GridBagConstraints.BOTH;
	    		gbc_outer.gridx = 0;
	    		gbc_outer.gridy = 0;
	    		mainPanel.add(outer, gbc_outer);
	    		
	    		//outer.setOneTouchExpandable(true);
	    		//mainPanel.add(outer, "2, 2, fill, fill");
	        	
	        	/*gbl.columnWidths = new int[]{0,0};
	    		gbl.rowHeights = new int[]{0,0};
	    		gbl.columnWeights = new double[]{1.0,1.0, Double.MIN_VALUE};
	    		gbl.rowWeights = new double[]{0.0,1.0, Double.MIN_VALUE};
	    		mainPanel.setLayout(gbl);*/
	    		generateProcessPanel();
	    		generateProcessInfo();
	    		generateglobalProcess();
	    		
	    		
	        }
		    
			
	     });
	       }
	/*
	 * create Upper Panel
	 */
	public void generateProcessPanel()
	{
		// create Upper main panel
		
		JPanel processPanel = new JPanel();
		outer.setLeftComponent(processPanel);
		GridBagLayout gbl_processPanel = new GridBagLayout();
		gbl_processPanel.columnWidths = new int[]{0, 0};
		gbl_processPanel.rowHeights = new int[]{0, 0};
		gbl_processPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_processPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		processPanel.setLayout(gbl_processPanel);
		
		/*
		 * split upper panel into two panels
		 */
		splitPane = new JSplitPane();
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 0;
		gbc_splitPane.gridy = 0;
		processPanel.add(splitPane, gbc_splitPane);
		
		/*
		 *  define upper left panel
		 */
		
		JPanel procPanel = new JPanel();
		splitPane.setLeftComponent(procPanel);
		GridBagLayout gbl_procPanel = new GridBagLayout();
		gbl_procPanel.columnWidths = new int[]{0, 0};
		gbl_procPanel.rowHeights = new int[]{0, 0, 0};
		gbl_procPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_procPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		procPanel.setLayout(gbl_procPanel);
		
		JLabel lblprocessInstances = new JLabel("Process Instances");
		lblprocessInstances.setFont(new Font("Arial", Font.BOLD, 17));
		//GridBagConstraints gbc_lblActivityInstances = new GridBagConstraints();
		GridBagConstraints gbc_lblprocessInstances = new GridBagConstraints();
		gbc_lblprocessInstances.insets = new Insets(0, 0, 5, 0);
		gbc_lblprocessInstances.gridx = 0;
		gbc_lblprocessInstances.gridy = 0;
		procPanel.add(lblprocessInstances, gbc_lblprocessInstances);
		
		JScrollPane scrollPaneProcess = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		procPanel.add(scrollPaneProcess, gbc_scrollPane);
		
		processTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		processTable.setFocusable(false);
		processTable.setRowSelectionAllowed(true);
		scrollPaneProcess.setViewportView(processTable);
		
		ListSelectionModel rowSelectionModelProcess = processTable.getSelectionModel();
		rowSelectionModelProcess.addListSelectionListener(new ListSelectionListener() 
		{
			public void valueChanged(ListSelectionEvent e) 
			{
			if (!e.getValueIsAdjusting()) 
			{
				if(getSelectedItem(processTable) !=null )
				{
					/*PMReloadProcessEvent prpe = new PMReloadProcessEvent("");
					prpe.addActionListener(controler);
					prpe.setProcessMonitoringView(pmv);
					prpe.setId(getSelectedItem1(processTable));
					prpe.doClick();*/
					
					renderProcessInfo();
					
					
				}
			}}
		});
		
		
	}
//-------------------------------------------------------------------------------------------------------------------------------
/*
 * Create upper right panel
 */
	public void generateProcessInfo()
	{
		JPanel processInfoPanel = new JPanel();
		
		splitPane.setRightComponent(processInfoPanel);
		GridBagLayout gbl_processInfoPanel = new GridBagLayout();
		gbl_processInfoPanel.columnWidths = new int[]{0, 0, 0};
		gbl_processInfoPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_processInfoPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_processInfoPanel.rowWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		processInfoPanel.setLayout(gbl_processInfoPanel);
		/*
		 * active tasks
		 */
		JLabel lblTaskInsLabel = new JLabel("Active Tasks");
		lblTaskInsLabel.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_lblTaskInsLabel = new GridBagConstraints();
		gbc_lblTaskInsLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblTaskInsLabel.gridx = 0;
		gbc_lblTaskInsLabel.gridy = 0;
		processInfoPanel.add(lblTaskInsLabel, gbc_lblTaskInsLabel);
		
		JScrollPane scrollPaneProcess = new JScrollPane();
		GridBagConstraints gbc_scrollPaneProcess = new GridBagConstraints();
		gbc_scrollPaneProcess.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPaneProcess.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneProcess.gridx = 0;
		gbc_scrollPaneProcess.gridy = 1;
		processInfoPanel.add(scrollPaneProcess, gbc_scrollPaneProcess);
		
		activeTaskTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		activeTaskTable.setFocusable(false);
		activeTaskTable.setRowSelectionAllowed(true);
		scrollPaneProcess.setViewportView(activeTaskTable);
		/*
		 * completed tasks
		 */
		JLabel lblprocessInstances1 = new JLabel("Completed Tasks");
		lblprocessInstances1.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_lblprocessInstances1 = new GridBagConstraints();
		gbc_lblprocessInstances1.insets = new Insets(0, 0, 5, 0);
		gbc_lblprocessInstances1.gridx = 1;
		gbc_lblprocessInstances1.gridy = 0;
		processInfoPanel.add(lblprocessInstances1, gbc_lblprocessInstances1);
		
		JScrollPane scrollPaneProcess1 = new JScrollPane();
		GridBagConstraints gbc_scrollPaneProcess1 = new GridBagConstraints();
		gbc_scrollPaneProcess1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPaneProcess1.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneProcess1.gridx = 1;
		gbc_scrollPaneProcess1.gridy = 1;
		processInfoPanel.add(scrollPaneProcess1, gbc_scrollPaneProcess1);
		
		completedTaskTabel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		completedTaskTabel.setFocusable(false);
		completedTaskTabel.setRowSelectionAllowed(true);
		scrollPaneProcess1.setViewportView(completedTaskTabel);
		/*
		 * active artifacts
		 */
		JLabel lblprocessInstances2 = new JLabel("Active Artifacts");
		lblprocessInstances2.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_lblprocessInstances2 = new GridBagConstraints();
		gbc_lblprocessInstances2.insets = new Insets(0, 0, 5, 5);
		gbc_lblprocessInstances2.gridx = 0;
		gbc_lblprocessInstances2.gridy = 2;
		processInfoPanel.add(lblprocessInstances2, gbc_lblprocessInstances2);
		
		JScrollPane scrollPaneProcess2 = new JScrollPane();
		GridBagConstraints gbc_scrollPaneProcess2 = new GridBagConstraints();
		gbc_scrollPaneProcess2.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPaneProcess2.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneProcess2.gridx = 0;
		gbc_scrollPaneProcess2.gridy = 3;
		processInfoPanel.add(scrollPaneProcess2, gbc_scrollPaneProcess2);
		
		activeArtifactTabel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		activeArtifactTabel.setFocusable(false);
		activeArtifactTabel.setRowSelectionAllowed(true);
		scrollPaneProcess2.setViewportView(activeArtifactTabel);
		/*
		 * completed artifacts
		 */
		JLabel lblprocessInstances3 = new JLabel("Completed Artifacts");
		lblprocessInstances3.setFont(new Font("Arial", Font.BOLD, 17));
		GridBagConstraints gbc_lblprocessInstances3 = new GridBagConstraints();
		gbc_lblprocessInstances3.insets = new Insets(0, 0, 5, 0);
		gbc_lblprocessInstances3.gridx = 1;
		gbc_lblprocessInstances3.gridy = 2;
		processInfoPanel.add(lblprocessInstances3, gbc_lblprocessInstances3);
		
		
		JScrollPane scrollPaneProcess3 = new JScrollPane();
		GridBagConstraints gbc_scrollPaneProcess3 = new GridBagConstraints();
		gbc_scrollPaneProcess3.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneProcess3.gridx = 1;
		gbc_scrollPaneProcess3.gridy = 3;
		processInfoPanel.add(scrollPaneProcess3, gbc_scrollPaneProcess3);
		
		completedArtifactTabel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		completedArtifactTabel.setFocusable(false);
		completedArtifactTabel.setRowSelectionAllowed(true);
		scrollPaneProcess3.setViewportView(completedArtifactTabel);
	}
//-------------------------------------------------------------------------------------------------------------------------------	
	/*
	 * create inner Panel
	 */
	public void generateglobalProcess()
	{
		JPanel globalProcessPanel = new JPanel();
		globalProcessPanel.setBackground(Color.WHITE);
		//GridBagLayout gbc = new GridBagLayout();
		outer.setRightComponent(globalProcessPanel);
		GridBagLayout gbl_globalProcessPanel = new GridBagLayout();
		gbl_globalProcessPanel.columnWidths = new int[]{0};
		gbl_globalProcessPanel.rowHeights = new int[]{0};
		gbl_globalProcessPanel.columnWeights = new double[]{Double.MIN_VALUE};
		gbl_globalProcessPanel.rowWeights = new double[]{Double.MIN_VALUE};
		globalProcessPanel.setLayout(gbl_globalProcessPanel);
		
	}
//---------------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------	
  	public void reloadProcessList(ProcessDef wid)
	{
  		//processTableModel.setRowCount(0);
  		processTable.setModel(processTableModel);
  		//for(int i=0;i<result.size();i++)
		{
  		
			processTableModel.addRow(new String[]{Long.toString(wid.processId), wid.name, wid.currentState, wid.initiator, wid.startDate.format(new Date()), "--","--"});
  			
		}
		processTable.setModel(processTableModel);
	}	
	public void reloadProcessList(List<ProcessDef> processList)
	{
		if(processList!=null)
		{
			processTableModel.setRowCount(0);
	  		for(int i=0;i<processList.size();i++)
			{
	  			processTableModel.addRow(new String[]{Long.toString(processList.get(i).processId), processList.get(i).name, processList.get(i).psm.getState().name()});
			}
		}
	}
	
	public void renderProcessInfo(List<MonitType> taskList)
	{
		
		if(taskList!=null)
		{
			activeTaskTableModel.setRowCount(0);
			completedTaskTabelModel.setRowCount(0);
			activeArtifactTabelModel.setRowCount(0);
			completedArtifactTabelModel.setRowCount(0);
	  		for(int i=0;i<taskList.size();i++)
			{
	  			System.out.println("sakdjaskljfklasj / " + taskList.get(i).state);
	  			/*if (taskList.get(i).state.equals("created")||taskList.get(i).state.equals("inProgress") )
	  			{
	  				activeTaskTableModel.addRow(new String[]{taskList.get(i).id, taskList.get(i).name, taskList.get(i).activityID, taskList.get(i).activityName,
	  						taskList.get(i).actor, taskList.get(i).state, taskList.get(i).start, taskList.get(i).end,taskList.get(i).duration});
	  				if (taskList.get(i).mta.name!=null)
	  				{
		  				if (taskList.get(i).mta.state.equals("defined") || taskList.get(i).mta.state.equals("outlined"))
			  			{
			  				completedArtifactTabelModel.addRow(new String[]{taskList.get(i).mta.id, taskList.get(i).mta.name,
			  						taskList.get(i).mta.actor, taskList.get(i).mta.state, taskList.get(i).mta.cp});
			  			}
			  			else
			  			{
			  				activeArtifactTabelModel.addRow(new String[]{taskList.get(i).mta.id, taskList.get(i).mta.name,
								taskList.get(i).mta.actor, taskList.get(i).mta.state, taskList.get(i).mta.cp});
			  			}
	  				}
		  				
	  			}
	  			else if (taskList.get(i).state.equals("completed"))
	  			{
	  				completedTaskTabelModel.addRow(new String[]{taskList.get(i).id, taskList.get(i).name, taskList.get(i).activityID, taskList.get(i).activityName,
	  						taskList.get(i).actor, taskList.get(i).state, taskList.get(i).start, taskList.get(i).end,taskList.get(i).duration});
	  				if (taskList.get(i).mta.name!=null)
	  				{
		  				if (taskList.get(i).mta.state.equals("defined") || taskList.get(i).mta.state.equals("outlined"))
			  			{
			  				completedArtifactTabelModel.addRow(new String[]{taskList.get(i).mta.id, taskList.get(i).mta.name,
			  						taskList.get(i).mta.actor, taskList.get(i).mta.state, taskList.get(i).mta.cp});
			  			}
			  			else
			  			{
			  				activeArtifactTabelModel.addRow(new String[]{taskList.get(i).mta.id, taskList.get(i).mta.name,
								taskList.get(i).mta.actor, taskList.get(i).mta.state, taskList.get(i).mta.cp});
			  			}
	  				}
	  			}*/
	  				
	  			
	  				
	  		}
	  		activeTaskTable.setModel(activeTaskTableModel);
	  		completedTaskTabel.setModel(completedTaskTabelModel);
	  		activeArtifactTabel.setModel(activeArtifactTabelModel);
	  		completedArtifactTabel.setModel(completedArtifactTabelModel);
	  			
		}
	  		
	}
	
	public void renderProcessInfo()
	{
		Date date = new Date();
		activeTaskTableModel.setRowCount(0);
		completedTaskTabelModel.setRowCount(0);
		activeArtifactTabelModel.setRowCount(0);
		completedArtifactTabelModel.setRowCount(0);
  		
  		activeTaskTableModel.addRow(new String[]{"2", "Define ELectrical Specification","1", "Detail Change Requirement",
  						"A1", "inProgress", date.toString(), "","3d"});
  				
		completedTaskTabelModel.addRow(new String[]{"2", "Define Component Requirements", "1", "Detail Change Requirement",
  						"A1", "completed", date.toString(), date.toString(),"2d"});
  		completedTaskTabelModel.addRow(new String[]{"3", "Outline Instrumentation Model", "1", "Detail Change Requirement",
  						"A1", "completed", date.toString(), date.toString(),"1/2d"});
  		
  			
		completedArtifactTabelModel.addRow(new String[]{"CR1", "Component Requirements",
		  						"A1", "defined", "100%"});
		completedArtifactTabelModel.addRow(new String[]{"FICD1", "FICD",
		  						"A1, IT1", "defined", "100%"});
		  			
		activeArtifactTabelModel.addRow(new String[]{"", "Electrical Specification",
							"A1", "defined", "8%"});
		 //******* 		
		 activeTaskTableModel.addRow(new String[]{"5", "Define Bench Specification", "3", "Generate Bench Specification",
  						"IT1", "completed", date.toString()," ","1d"});
	  	completedTaskTabelModel.addRow(new String[]{"4", "Define Instrumentation Model", "2", "Design Instrumentation",
  						"IT1", "completed", date.toString(), date.toString(),"1/2d"});
  		activeArtifactTabelModel.addRow(new String[]{"", "Bench Specification",
							"IT1", "defined", "4%"});
		//********************
		 
		 completedTaskTabelModel.addRow(new String[]{"6", "Purchase Components", "4", "Supply Components",
  						"S1", "completed", date.toString(), date.toString(),"1d"});
		 
		 activeTaskTableModel.addRow(new String[]{"7", "Fix Components", "5", "Fix and Install Components",
  						"WT1", "inProgress", date.toString(), "","2d"});
		
	  		activeTaskTable.setModel(activeTaskTableModel);
	  		completedTaskTabel.setModel(completedTaskTabelModel);
	  		activeArtifactTabel.setModel(activeArtifactTabelModel);
	  		completedArtifactTabel.setModel(completedArtifactTabelModel);
	  			
		
	  		
	}
//----------------------------------------------------------------------------------------------------------------
	public String getSelectedItem(JTable table) 
  	{
  		int column = 1;
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String artifact = table.getModel().getValueAt(index, column).toString();
  			return artifact;
  		}
  		return null;
  	}
	
	public String getSelectedItem1(JTable table) 
  	{
  		int column =0;
  		int index = table.getSelectedRow();
  		if (index != -1) 
  		{
  			String artifact = table.getModel().getValueAt(index, column).toString();
  			return artifact;
  		}
  		return null;
  	}
	
}
