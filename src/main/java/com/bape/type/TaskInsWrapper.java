package com.bape.type;



public class TaskInsWrapper 
{
	public TaskType task;
	long id;
	
	public TaskInsWrapper(long id, TaskType task) 
	{
		this.task = task;
		this.id = id;
	}
	
	
	public TaskType getWorkItem() 
	{
		return task;
	}
	public long getWorkItemm() 
	{
		return id;
	}
	
	
	public String toString() 
	{
		return  "[" + id + "] " + task.type + " " +task.state ;
	}
	
}
