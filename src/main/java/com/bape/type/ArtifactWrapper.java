package com.bape.type;



public class ArtifactWrapper 
{
	public TaskType task;
	public ArtifactType artifact;
	
	public ArtifactWrapper(TaskType task, ArtifactType artifact) 
	{
		this.task = task;
		this.artifact = artifact;
	}
	
	
	public TaskType getWorkItem() 
	{
		return task;
	}
	
	public ArtifactType getArtifact() 
	{
		return artifact;
	}
	
	public String toString() 
	{
		return  artifact.type + "   " +  artifact.state + "   " + artifact.usage;
	}
	
	/*public ArtifactWrapper(WorkItem workItem, TaskType task, ArtifactType artifact) 
	{
		this.task = task;
		this.workItem = workItem;
		this.artifact = artifact;
	}
	
	
	public TaskType getWorkItem() 
	{
		return task;
	}
	public WorkItem getWorkItemm() 
	{
		return workItem;
	}
	public ArtifactType getArtifact() 
	{
		return artifact;
	}
	
	public String toString() 
	{
		return  artifact.type + "   " +  artifact.state + "   " + artifact.usage;
	}*/
}
