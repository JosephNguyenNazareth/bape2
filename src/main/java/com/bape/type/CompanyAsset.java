package com.bape.type;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.bape.model.ActivityStateMachine;
import com.bape.model.TaskStateMachine;

public class CompanyAsset
{
	public List<String> processList;
	public List<String> roleList;
//	public List<String> artifactList;
	private HashMap<String, List <String>> role2AtrifactListMap = new HashMap<String, List <String>>();
	//private HashMap<String, List <String>> artifact2StateListMap = new HashMap<String, List <String>>();
	
	
	public CompanyAsset()
	{
		//artifactList = new ArrayList<String>();
		processList = new ArrayList<String>();
		roleList = new ArrayList<String>();
	}
	
	public void createArtifact(String role)
	{
		List <String> artifactList = new ArrayList<String>();
		getRole2AtrifactListMap().put(role, artifactList);
		//getArtifact2StateListMap().put(artifact, stateList);
	}
	
	public HashMap<String, List <String>> getRole2AtrifactListMap() {
		return role2AtrifactListMap;
	}

	public void setRole2AtrifactListMap(HashMap<String, List <String>> role2AtrifactListMap) {
		this.role2AtrifactListMap = role2AtrifactListMap;
	}
	/*public HashMap<String, List <String>> getArtifact2StateListMap() {
		return artifact2StateListMap;
	}

	public void setArtifact2StateListMap(HashMap<String, List <String>> artifact2StateListMap) {
		this.artifact2StateListMap = artifact2StateListMap;
	}*/
}
