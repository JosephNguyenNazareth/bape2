package com.bape.type;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class generateXML
	{
		public String processPath;
		File fXmlFile;
		DocumentBuilderFactory dbFactory;
		DocumentBuilder dBuilder;
		public Document doc;
		
		public generateXML(String processPath)
		{
			// read the Process Fragment
    	 	this.processPath = processPath;
			fXmlFile = new File(processPath);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = null;
			try {
				dBuilder = dbFactory.newDocumentBuilder();
			} catch (ParserConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			doc = null;
			
			try {
				doc = dBuilder.parse(fXmlFile);
			} catch (SAXException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			doc.getDocumentElement().normalize();
			
		}
	}