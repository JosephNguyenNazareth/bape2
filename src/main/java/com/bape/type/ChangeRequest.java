package com.bape.type;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChangeRequest 
{
	public humanResourceType initHres, resHres;
	public String  ID, projID, processName, task, initComment, resComment;
	public List<ArtifactType> artifactList;
	public List<humanResourceType> affectedHres; 
	public List<ArtifactType> affectedArtifact;
	public Date date;
	public ChangeRequest()
	{
		artifactList = new ArrayList<ArtifactType>();	
		initHres = new humanResourceType();
		resHres = new humanResourceType();
	}
}
