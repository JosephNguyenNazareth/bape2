package com.bape.type;

public class TaskItemDefWrapper {

	public TaskType task;
	
	public TaskItemDefWrapper(TaskType task) 
	{
		this.task = task;
	}
	
	
	public TaskType getWorkItem() 
	{
		return task;
	}
	
	
	public String toString() 
	{
		
	
		return   task.type ;
	}
}