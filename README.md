## NOTICE FOR RUNNING BAPE FROM THE START, NOT THIS PROJECT

*This project has been modified to be run smoothly on Eclipse at least 2020-09*

1. Prepare at least Eclipse 2020-09, and download the package m2e in order to transform the eclipse project to maven project
2. Ensure that the project has been loaded as maven project when right click on the project name, there is a maven selection.
3. Choose global JRE as 1.8, by selecting Windows->Preferences->Java->Installed JREs and select your JRE version 1.8 in your directory. Restart required for applying this change.
4. Right click the project name, choose Properties->Java Build Path, ensure that the JRE system library matched with the global setting.
5. Search on the .classpath file, check for the local package name used in the project then search on the maven repository for corresponding package (notice on the version), copy these into the pom.xml. After having copying all the necessary package, right click on the project name, choose Maven->Update Projects->OK and packages will be downloaded.
6. Open src->main->java->com->bape->controller->ProcessMain. Right click on the editor choose Run As->Java Application and the project will be built. There will be errors on some variables due to missing keyword final, be patient to add this keyword for all the error variables.
7. Run again, check at the Console window if you are running the correct version of java as setting (in case some ambiguous settings). In the regular case, the proejct will run smoothly.

